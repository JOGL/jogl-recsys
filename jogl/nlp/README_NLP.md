# Overview

The NLP part of the jogl recommender aims to reconciliate all possibilities of writing a skill under a golden record.
So far, it allows to:
* Annihilate effects of uppercase and punctuation symbols
* Tackle basic typos (missing a character, switch of two characters, ...)
* Identify declared acronyms in parentheses
* Reconciliate acronyms and full lenght names based on the context (other skills associated)
* Unify different langages under their english version (French, Spanish and German are translated)
* Remove temporal, feminine or plural parts of skills

The golden record of a skill is the version expressed in the intial file used by JOGL (took from a LinkdedIn referential) or (if not not in the previous referential) it's the most popular spelling based on the statistical count in project needs and users skills.

# Usage

## As a docker

The cleaning process is included in the recsys process, you can refer to the main README for launching the dockerfile of the whole project.

## As a python library

You can find in the `jogl\nlp\samples` a jupyter notebook giving an example of usage on its own to import it as a library.
Important: The example runs with old extracts of jogl database. It's not and will not be updated.

# Main structure

The NLP part is based on 2 python files and 1 model, so far.

The `engine.py` (which is in the `jogl\nlp` folder) contains all the steps for initialization, fitting and appying processes where all sub-functions treating specific tasks are comming from the `nlp_functions.py` file.

The process also use pre-trained models:
* One is fixed and stored in the `jogl\nlp\models` folder (language identification)
* Few others are downloaded from [huggingface.co](https://huggingface.co/transformers/model_doc/marian.html) and not stored (language translation).

# Infos

For more details you can refer:
* functions docstrings for everything related to usage
* the NLP_Documentation.md for a better understanding of steps and gor the general overview from a technical point of view