import pandas as pd
import boto3
import os

from jogl.nlp.stores import to_nlp, to_jogl, Store

# THIS CODE IS NOT TESTED
# USE WITH CAUTION

class AwsFiles(Store):
    def __init__(self):
        super().__init__()
        # Setup boto3 environment variables
        os['AWS_ACCESS_KEY_ID'] = getenv('AWS_S3_KEY_ID')
        os['AWS_SECRET_ACCESS_KEY'] = getenv('AWS_S3_SECRET')
        os['AWS_DEFAULT_REGION'] = getenv('AWS_S3_REGION')
        # And AWS variables
        self.AWS_S3_BUCKET = getenv("AWS_S3_BUCKET")
        self.need_back_up = bool(getenv("AWS_S3_BACKUP"))
        self.AWS_S3_PREFIX = 'jogl-nlp/'

    def load_skills(self):
        self.skills = self.readfroms3('skills.json')
        self.skills.rename(to_nlp, axis=1, inplace=True)

        return self.skills

    def load_skills_referential(self):
        self.skills_referential = self.readfroms3('skills_referential.json')
        self.skills_referential.rename(to_nlp, axis=1, inplace=True)

        return self.skills_referential

    def load_acronyms_referential(self):
        self.acronyms_referential = self.readfroms3('acronyms_referential.json')
        self.acronyms_referential.rename(to_nlp, axis=1, inplace=True)

        return self.acronyms_referential

    def save_skills(self, backup=False):
        if len(self.skills) > 0:
            df = self.skills.rename(to_jogl, axis=1)
            self.backup_writes3(df, 'skills.json')

    def save_skills_referential(self, backup=False):
        if len(self.skills_referential) > 0:
            df = self.skills_referential.rename(to_jogl, axis=1)
            self.backup_writes3(df, 'skills_referential.json')

    def save_acronyms_referential(self, backup=False):
        if len(self.acronyms_referential) > 0:
            df = self.acronyms_referential.rename(to_jogl, axis=1)
            self.backup_writes3(df, 'acronyms_referential.json')

    # --- WORKING WITH AWS S3 --- #

    def readfroms3(self, s3_file_name):
        """
        Function to read a file from a S3 AWS bucket into a dataframe

        Inputs:
            s3_file_name: str, name of the file into S3 which contains the dataframe

        Output:
           dataframe with content of the file stored into S3.
        """

        # Connect to S3
        s3 = boto3.resource(service_name='s3')

        # Get dataframe from s3 file
        obj = s3.Bucket(self.AWS_S3_BUCKET).Object(self.AWS_S3_PREFIX + s3_file_name).get()

        if '.csv' in s3_file_name:
            df_obj = pd.read_csv(obj['Body'], index_col=0)

        elif '.txt' in s3_file_name:
            df_obj = obj['Body'].read()

        elif '.json' in s3_file_name:
            df_obj = pd.read_json(json.loads(obj['Body'].read().decode('utf-8')))

        else:
            print("File is neither a .csv, .txt nor .json")
            return ''

        return df_obj


    def writetos3(self, df, s3_file_name):
        """
        Function to write (or overwrite) a pandas dataframe into a S3 AWS bucket.

        Inputs:
            df: dataframe, dataframe to write into S3
            s3_file_name: str, name of the file into S3 which (will) contains the dataframe

        Output:
            None, write object into a AWS S3 bucket

        """

        # Connect to S3
        s3 = boto3.resource(service_name='s3')

        if ".csv" in str(s3_file_name):

            # Get dataframe to csv proxy
            csv_buffer = StringIO()
            df.to_csv(csv_buffer)

            # Write into S3
            s3.Object(self.AWS_S3_BUCKET, s3_file_name).put(Body=csv_buffer.getvalue())

        elif ".json" in str(s3_file_name):
            s3.Object(self.AWS_S3_BUCKET, s3_file_name).put(
                Body=(bytes(json.dumps(df.to_json()).encode('UTF-8'))))


    def create_backups3(self, s3_file_name):
        """
        Function to create a back-up of a file in a S3 bucket. The new file name will be preceed by "backup_".

        Inputs:
            s3_file_name: str, name of the file into S3 which contains the dataframe

        Output:
           None, copy object into the AWS S3 bucket

        """

        s3 = boto3.resource(service_name='s3')

        copy_source = {
            'Bucket': self.AWS_S3_BUCKET,
            'Key': s3_file_name}

        bucket = s3.Bucket(self.AWS_S3_BUCKET)
        backup_name = 'backup_' + s3_file_name
        bucket.copy(copy_source, backup_name)


    def backup_writes3(self, df, file_s3):

        """
           Function to create a back-up of a file in a S3 bucket and write a pandas dataframe into a S3 AWS bucket.
           Based on "create_backups3" and "writetos3". Back-up process is optional.

           Inputs:
               df: dataframe, 1st dataframe to write into S3
               file_s3: str, name of the file into S3 which (will) contains the dataframe

           Output:
               None, write object into a AWS S3 bucket

           """
        # Create back-up
        if self.need_back_up is True:
            try:
                create_backups3(s3_file_name=file_s3)

            except ClientError:
                print('No file "{}" to save as back-up.'.format(file_s3))
                pass

        # Writes files into S3
        writetos3(df=df, s3_file_name=file_s3)
