import pandas as pd
import os

from sqlalchemy import create_engine, update
from sqlalchemy import MetaData
from sqlalchemy import Table
from sqlalchemy.sql.expression import bindparam
from sqlalchemy.orm import (scoped_session, sessionmaker)

from sqlalchemy import Column, Integer, String, Boolean, Float, DateTime
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
from sqlalchemy.sql import func

import datetime

from jogl.nlp.stores import to_nlp, to_jogl, Store

class Skill(Base):
    __tablename__ = 'skills'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    skill_name = Column(String(1000))
    clean_skill_name = Column(String(1000))
    enriched_skill_name = Column(String(1000))
    stemmed_skill_name = Column(String(1000))
    is_linkedin_skill = Column(Boolean)
    count = Column(Integer)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

class SkillReferential(Base):
    __tablename__ = 'recsys_skills_referential'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    skill_name = Column(String(1000))
    clean_skill_name = Column(String(1000))
    enriched_skill_name = Column(String(1000))
    stemmed_skill_name = Column(String(1000))
    lg_skill = Column(String(1000))
    is_linkedin_skill = Column(Boolean)
    count = Column(Integer)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

class Acronym(Base):
    __tablename__ = 'acronyms'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    acronym_name = Column(String(1000))
    probable_skill_name = Column(String(1000))
    probability = Column(Float)
    is_in_parenthesis = Column(Boolean)
    count = Column(Integer)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

class RecsysDatum(Base):
    __tablename__ = 'recsys_data'
    __table_args__ = {'extend_existing': True}
    id = Column(Integer, primary_key=True)
    sourceable_node_type = Column(String(1000))
    sourceable_node_id = Column(Integer)
    targetable_node_type = Column(String(1000))
    targetable_node_id = Column(Integer)
    relation_type = Column(String(1000))
    value = Column(Integer)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())

class JoglDatabase(Store):
    def __init__(self, network_id=None):
        '''
        Initialize the JoglDatabase instance.  The DATABASE_URL environment
        variable must be defined.
        '''
        super().__init__()
        self.engine = create_engine(os.environ['DATABASE_URL'])

    def upsert(self, model, df, old_df):
        db_session = scoped_session(sessionmaker(bind=self.engine))
        changed_df = old_df.merge(df, indicator=True, how='outer')
        changed_df = changed_df[changed_df['_merge'] == 'right_only']
        changed_df.drop('_merge',axis=1,inplace=True)
        if 'skill_id' in changed_df:
            del(changed_df['skill_id'])
        if 'created_at' in changed_df:
            changed_df['created_at'] = changed_df['created_at'].astype(object).where(changed_df['created_at'].notnull(), datetime.datetime.utcnow())
        if 'updated_at' in changed_df:
            changed_df['updated_at'] = changed_df['updated_at'].astype(object).where(changed_df['updated_at'].notnull(), datetime.datetime.utcnow())
        if 'count' in changed_df:
            changed_df['count'] = changed_df['count'].astype(object).where(changed_df['count'].notnull(), 0)
        table_dict = changed_df.to_dict(orient="records")
        for row in table_dict:
            rec = model(**row)
            db_session.merge(rec)
        db_session.commit()

    def load_network(self):
        '''
        Build a pandas DataFrame from the recsys_data table which has the following
        structure:

        id: bigint
        sourceable_node_type: string
        sourceable_node_id: bigint
        targetable_node_type: string
        targetable_node_id: bigint
        relation_type: string
        value: bigint
        created_at: timestamps
        updated_at: timestamps

        We use a mapping to rename columns to the names expected by NLP.
        '''
        with self.engine.connect() as connection:
            # Read recsys_data table into a DataFrame
            df = pd.read_sql_table('recsys_data', connection)

            # Rename DataFrame columns to what is expected by hinpy
            self.network = df
            return self.network

    def load_skills(self):
        '''
        Build a pandas DataFrame from the skills table which has the following
        structure:

        id type: bigint
        skill_name type: string
        clean_skill_name type: string
        enriched_skill_name type: string
        stemmed_skill_name type: string
        is_linkedin_skill type: boolean
        count type: bigint

        We use a mapping to rename columns to the names expected by NLP.
        '''
        with self.engine.connect() as connection:
            # Read recsys_data table into a DataFrame
            df = pd.read_sql_table('skills', connection)

            # Rename DataFrame columns to what is expected by hinpy
            df.rename(to_nlp, axis=1, inplace=True)
            self.skills = df
            return self.skills

    def load_skills_referential(self):
        '''
        Build a pandas DataFrame from the recsys_skills_referential table which has the following
        structure:

        id type: bigint
        skill_name type: string
        clean_skill_name type: string
        enriched_skill_name type: string
        stemmed_skill_name type: string
        is_linkedin_skill type: boolean
        count type: bigint

        We use a mapping to rename columns to the names expected by NLP.
        '''
        with self.engine.connect() as connection:
            # Read recsys_data table into a DataFrame
            df = pd.read_sql_table('recsys_skills_referential', connection)

            # Rename DataFrame columns to what is expected by hinpy
            df.rename(to_nlp, axis=1, inplace=True)
            self.skills_referential = df
            return self.skills_referential

    def load_acronyms_referential(self):
        '''
        Build a pandas DataFrame from the acronyms table which has the following
        structure:

        id type: bigint
        acronym_name type: string
        probable_skill_name type: string
        probability type: float
        is_in_parenthesis type: boolean
        count type: bigint

        We use a mapping to rename columns to the names expected by NLP.
        '''
        with self.engine.connect() as connection:
            # Read recsys_data table into a DataFrame
            df = pd.read_sql_table('acronyms', connection)

            # Rename DataFrame columns to what is expected by hinpy
            df.rename(to_nlp, axis=1, inplace=True)
            self.acronyms_referential = df
            return self.acronyms_referential

    def save_stemmed_skills(self):
        '''
        Saves the dataframes passed as arguments to the skills table.
        Previous data in the skills table is updated by the new one. DataFrame's will
        have their columns renamed to what is expected by the skills table.
        '''
        # This SQL wiwardry should do the trick as it updates inplace the values without destroying anything.
        # This was conceived from two pages: https://stackoverflow.com/questions/25694234/bulk-update-in-sqlalchemy-core-using-where
        # https://www.pythonsheets.com/notes/python-sqlalchemy.html
        ########################################################
        # if len(self.skills) > 0:
        #     df = self.skills.rename(to_jogl, axis=1)
        #
        #     with self.engine.connect() as connection:
        #         # Read recsys_data table into a DataFrame
        #         old_df = pd.read_sql_table('skills', connection)
        #
        #     self.upsert(Skill, df, old_df)
        print("Saving the results back to the DB is Not implemented")

    def save_skills_referential(self):
        '''
        Saves the dataframes passed as arguments to the skills_referential table.
        Previous data in the skills_referential table is updated by the new one. DataFrame's will
        have their columns renamed to what is expected by the skills_referential table.
        '''
        # This SQL wiwardry should do the trick as it updates inplace the values without destroying anything.
        # This was conceived from two pages: https://stackoverflow.com/questions/25694234/bulk-update-in-sqlalchemy-core-using-where
        # https://www.pythonsheets.com/notes/python-sqlalchemy.html
        ########################################################
        if len(self.skills_referential) > 0:
            df = self.skills_referential.rename(to_jogl, axis=1)
            if 'id' not in df:
                df['id'] = range(len(df))

            with self.engine.connect() as connection:
                # Read recsys_data table into a DataFrame
                old_df = pd.read_sql_table('recsys_skills_referential', connection)

            self.upsert(SkillReferential, df, old_df)

    def save_stemmed_network(self):
        print("Saving the results back to the DB is Not implemented")

    def save_acronyms_referential(self):
        '''
        Saves the dataframes passed as arguments to the acronyms table.
        Previous data in the skills table is updated by the new one. DataFrame's will
        have their columns renamed to what is expected by the skills table.
        '''
        # This SQL wiwardry should do the trick as it updates inplace the values without destroying anything.
        # This was conceived from two pages: https://stackoverflow.com/questions/25694234/bulk-update-in-sqlalchemy-core-using-where
        # https://www.pythonsheets.com/notes/python-sqlalchemy.html
        ########################################################
        if len(self.acronyms_referential) > 0:
            df = self.acronyms_referential.rename(to_jogl, axis=1)
            if 'id' not in df:
                df['id'] = range(len(df))

            with self.engine.connect() as connection:
                # Read recsys_data table into a DataFrame
                old_df = pd.read_sql_table('acronyms', connection)

            self.upsert(Acronym, df, old_df)
