import pandas as pd


class Store:
    """
    This is the base Store class for the skills NLP system, it will be used to be extended by different data stores.
    Each datastore will have at minima results and data to hold the datasets.
    Each datastore must extend the load_data and save_results methods.

    The definition of the dataframes column are as follow:
    self.skills:
    raw_skill | clean_skill | enrich_skill | stem_skill | isinitref | flag

    raw_skill : str - user entered skill
    clean_skill : str - skill after cleaning (lower, trim, parenthesis removal)
    enrich_skill : str - itermediate value for the skill (translated and acronyms_referential mapped)
    stem_skill : str - final value for the skill (stemmed)
    isinitref : int/bool - is the skill presentin the LinkedIn skill reference
    flag: int - Number of time the skill is present in the DB

    self.acronyms_referential:
    acronym | meaning | declared | proba | flag

    acronym : str - acronym
    meaning : str - clean_skill mapping of the acronym
    declared: int/bool - Was this declared within parentheses or not
    proba : float - estimated probability based on ensemble similarity
    flag : int - Number of occurences
    """

    def __init__(self):
        self.network = pd.DataFrame()
        self.skills = pd.DataFrame()
        self.skills_referential = pd.DataFrame()
        self.acronyms_referential = pd.DataFrame()

    def clean_skills(self):
        self.skills = self.skills[~self.skills['raw_skill'].isna()]

    def load_network(self):
        '''
        Obtain data from the specified source and create the network DB store.
        '''
        raise Exception(
            "Please extend the load_network method in your custom store.")

    def load_skills(self):
        '''
        Obtain data from the specified source and create the skills DB store.
        '''
        raise Exception(
            "Please extend the load_skills method in your custom store.")

    def load_skills_referential(self):
        '''
        Obtain data from the specified source and create the skills referential DB store.
        '''
        raise Exception(
            "Please extend the load_skills_referential method in your custom store.")

    def load_acronyms_referential(self):
        '''
        Obtain data from the specified source and create the acronyms_referential DB store.
        '''
        raise Exception(
            "Please extend the load_acronyms_referential method in your custom store.")

    # Not needed for now
    #def save_network(self):
    #    '''
    #    Save the updated skills DataFrame to the given store.
    #    '''
    #    raise Exception(
    #        "Please extend the save_skills method in your custom store.")

    def save_stemmed_network(self):
        '''
        Save the updated skills DataFrame to the given store.
        '''
        raise Exception(
            "Please extend the save_stemmed_network method in your custom store.")

    def save_stemmed_skills(self):
        '''
        Save the updated skills DataFrame to the given store.
        '''
        raise Exception(
            "Please extend the save_stemmed_skills method in your custom store.")

    def save_skills_referential(self):
        '''
        Save the updated skills referentials to the given store.
        '''
        raise Exception(
            "Please extend the save_skills_referential method in your custom store.")

    def save_acronyms_referential(self):
        '''
        Save the updated acronyms_referential mapping DataFrame to the given store.
        '''
        raise Exception(
            "Please extend the save_acronyms_referential method in your custom store.")
