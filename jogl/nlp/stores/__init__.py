to_nlp = {
    'skill_id': 'id',
    'skill_name': 'raw_skill',
    'clean_skill_name': 'clean_skill',
    'enriched_skill_name': 'enrich_skill',
    'stemmed_skill_name': 'stem_skill',
    'is_linkedin_skill': 'isinitref',
    'count': 'flag',
    'probability': 'proba',
    'is_in_parenthesis': 'declared',
    'probable_skill_name': 'meaning',
    'acronym_name': 'acronym'
}

to_jogl = {v: k for (k, v) in to_nlp.items()}

nlp_names = to_nlp.keys()

jogl_names = to_nlp.values()

# From Leo : To Do fix other stores
from .store import Store as Store
from .csv import CsvFiles as CSVStore
from .database import JoglDatabase as DatabaseStore
#from .aws import AwsFiles as AWSStore
