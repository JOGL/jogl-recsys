import pandas as pd

from jogl.nlp.stores import to_nlp, to_jogl, Store


class CsvFiles(Store):
    def __init__(self, skills_file, network_file, skills_referential_file, acronyms_referential_file):
        super().__init__()
        self.skills_file = skills_file
        self.network_file = network_file
        self.skills_referential_file = skills_referential_file
        self.acronyms_referential_file = acronyms_referential_file

    def load_network(self):
        self.network = pd.read_csv(self.network_file)
        #self.network.rename(to_nlp, axis=1, inplace=True)
        return self.network

    def load_skills(self):
        self.skills = pd.read_csv(self.skills_file)
        self.skills.rename(to_nlp, axis=1, inplace=True)
        self.clean_skills()
        return self.skills

    def load_skills_referential(self):
        self.skills_referential = pd.read_csv(self.skills_referential_file)
        self.skills_referential.rename(to_nlp, axis=1, inplace=True)
        return self.skills_referential

    def load_acronyms_referential(self):
        self.acronyms_referential = pd.read_csv(self.acronyms_referential_file)
        self.acronyms_referential.rename(to_nlp, axis=1, inplace=True)
        return self.acronyms_referential

    def save_acronyms_referential(self):
        if len(self.acronyms_referential) > 0:
            df = self.acronyms_referential.rename(to_jogl, axis=1)
            # /!\ Je ne pense pas qu'il faut renommer le fichier. Car c'est un referentiel qui s'update.
            self.acronyms_referential.to_csv(self.acronyms_referential_file.replace('.csv', '_out.csv'), index=False)

    def save_skills_referential(self):
        if len(self.skills_referential) > 0:
            df = self.skills_referential.rename(to_jogl, axis=1)
            # /!\ Je ne pense pas qu'il faut renommer le fichier. Car c'est un referentiel qui s'update.
            df.to_csv(self.skills_referential_file.replace('.csv', '_out.csv'), index=False)

    def save_stemmed_skills(self):
        if len(self.skills) > 0:
            df = self.skills.rename(to_jogl, axis=1)
            df.to_csv(self.skills_file.replace('.csv', '_out.csv'), index=False)

    def save_stemmed_network(self):
        if len(self.network) > 0:
            #df = self.network.rename(to_jogl, axis=1)
            self.network.to_csv(self.network_file.replace('.csv', '_out.csv'), index=False)
