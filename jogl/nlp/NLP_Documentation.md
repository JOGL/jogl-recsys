# Documentation of the NLP part

## Overview and Summary

### Main purposes

The current nlp module aims to clean the free-entry skill fields in the JOGL user profile and/or in needs asked in projects.

This is a back-end process at database level which do not impact the user experience. Skills are processed and used in their clean form only for algorithmic purposes (like the recommender system Hinpy).

### Summary

* Process Sschema and explanations
* Decisions and constraints
* To do / To improve


## Process schema and explanations

### Schema pictures

![Initialization Schema](./docs/Initialize.jpg)

![Learning Schema](./docs/Learn.png)

![Applying Schema](./docs/Applying.jpg)
![Algolia Schema](./docs/Algolia.jpg)

### Detailed explanations

#### Part 1 : Initialization Process.

This part is used to initiate one referential of skills and one
referential of abbreviations. It's based on a skill list obtained from a
LinkedIn popular skills extract; this same file was used to populate the
skills the JOGL interface of skill fields (pre-selected skills after few
typed letters) at the beginning of the project. Note that now skills are
enhanced by JOGL user skills.

- __"Basic Cleaning"__
  - Description: Simple basic steps of text cleaning. Lowercase, remove
    unnecessary spaces replace some specific character ("&", "-") and
    few hand selected words ("datum" -> "data", "medium" -> "media") .

* __"Extract Declared Acronyms"__ 
	* Description: This step matches characters in parentheses with firsts letters of each words outside of parentheses. If there is a match characters in parentheses are considered as the acronym of the skill otherwise parenthesis is ignored.
	* Examples: "_a_/sset _b_/acked _s_/ecurity (abs)" defines "abs" as an acronym for "asset backed security" but "_a_/sset _b_/acked _s_/ecurity (beginner)" just ignore the parenthesis and do not create any potential abbreviation.

- __"Remove Parentheses"__
    - Description: Simply remove parentheses and their contents from
      skills. Also remove unnecessary space due to the parenthesis’s
      removal. It's not used in the same function as the basic cleaning
      due to specific treatments which have to be made in it (like
      "Extract Declared Acronyms").

* __"Remove Acronyms"__
	* Description: At this point of the initialization process, extracted acronyms come from declared ones (Example: "full-skill (acronym)") where each acronym is associated to its full skill. So, to avoid duplicates of cleaned full skills and some acronyms which also may be present in the input referential.
	* Examples: A skill "machine learning (ml)" present in the input list will associate both parts "machine learning" and "ml". Then, to prevent any duplicates, all acronyms found (like "ml") are removed from the list of full skills (in case of the list contains "ml" as skill).

- __"Cleaning Issues"__
    - Description: This step remove skill which contain no alpha
      character. If not removed they can generate some crashes.

* __Language identification"__
	* Step 1/3 - Find software, tools, languages, etc.:
		* Description: This step treats skills that are containing numbers or skills that are partially/fully uppercased. A quick overview of skills with this pattern shows that these skills are almost all referring to a technology which do not have to be translated (SQL, A-110, JavaScript, etc.). 
		To be considered as a _technology skill_, the skill must contain at least 1 number and/or at least as many uppercase characters than distinct words.
		* Examples: "JavaScript" contains 2 uppercase characters for only one word, then it's considered as a _technology skill_ and do not have to be translated. However, "Data management" which contains only 1 uppercase characters for 2 words will not be considered as a _technology skill_.

	* Step 2/3 - Identify the language of skills:
		* Description: There was no open-source package or pretrained model able to perform a language detection and translation. Then we used a __fastText pre-trained model__ to do the identification part.
		The model in use is in the "models" folder of this project (under `jogl/nlp`). For a given string to identify the model return a list of potential languages and a confidence estimator. Since the list is ordered by decreasing confidence level, we are taking the first one and comparing the confidence value associated to a manually defined threshold (see more in __To do/To improve__ section).
		* Example: If "Bonjour" is identified as French with a confidence level superior to 0.5 (which is our default threshold) then the language is set at "fr" for French. Otherwise, if "Kenavo" is identified as French with a confidence level superior of 0.1 (which is under our default threshold of 0.5) then the language is set at "unk" for unknown.

- __"Language Translation"__
    - Description: There was no open-source package or pretrained model
       able to perform a language detection and translation. Then we
       used few __Marian pre-trained transformers (from Huggingface)__
       to do the translation part. After a quick analysis of skills
       languages, we identified mostly 3 foreign languages to translate:
       French, German and Spanish. In the dedicated function, 2 Marian
       transformers are loaded (1 for German, 1 for French & Spanish)
       and used on skills. However, there is one condition to allow the
       translation: the translation the foreign skill must exist in the
       English skills already present (it can be a technology based on a
       foreign language, see examples).
    - Examples: "Gestion de projets" is identified as French in step 2
      and will be translated to "Project Management" since this English
      skill is already present in the list of skills. "Kubernetes" is
      identified as Estonian in step 2 and (if we were also dealing with
      the Estonian language) it would be translated as "Governors" but
      since the skill "Governors" is not present in the list, it's not
      translated

* __"Stemming"__
	* Description: Apply a Porter Stemmer from package _nltk_ to get roots of words for all skills (remove plurals, -ing, etc.)

- __"Create skills referential"__
    - Description: Create a output datafile (csv, db, etc.) according to
      the store in use. The "skills referential" file has the following
      columns:
      - __raw_skill__: Skill in its raw syntax as it is in the
        JOGL databases.
      - __clean_skill__: Skill cleaned from basic
        text-mining manipulations.
      - __lg_skill__: Skill language identified.
      - __enrich_skill__: Skill translated (English as basis)
      - __stem_skill__: Skill under its stemming form (after all
        treatments). Final result of cleaning process. - __flag__: int,
        count occurrences in the JOGL environment (user, project, need,
        etc.) of the skill. At the end of the "initialize_referential"
        function the whole column is set to 0. - __isinitref__: Boolean,
        inform of the presence of the skill in the input list. At the
        end of the "initialize_referential" function the whole column is
        set to 1.

* __"Create acronyms referential"__
	* Description: Create a output datafile (csv, db, etc.) according to the store in use.
	The "acronyms referential" file has the following columns:
		- __meaning__: full skill
		- __acronym__: identified acronym
		- __flag__: int, TBD/Check
		- __declared__: Boolean, inform if the acronym has been deduced from the skill itself (in the parentheses) or not (like from the context). At the end of the "initialize_referential" function the whole column is set to 1 since they are all declared in parentheses.
		- __proba__: float, 0 to 1, some kind of confidence index in the meaning-acronym association. Set to 0.2 (arbitrarily) for associations from skill deduction (in the parentheses).


#### Part 2 : Learning 

This part is used to enrich and maintain referentials up to date each
time it’s used. It's only based on JOGL skills (expressed in projects
needs or in users’ skills). Since there is lot of skills to process a
statistical approach of the problem is in use (models, occurrences,
etc.). This part is supposed to be launch on a regular basis (weekly or
monthly) to be more accurate with the evolution of the platform (new
skills, new domains for projects, etc.)

* __"Extracts"__
  * Description: Extracting data from JOGL bases. One file for the
    acronyms referential, on file for skills referential and one file
    for used skills in JOGL (users, projects, needs, etc.)

- __"Extract Declared Acronyms"__
  - Description: This step matches characters in parentheses with firsts
    letters of each words outside of parentheses. If there is a match
    characters in parentheses are considered as the acronym of the skill
    otherwise parenthesis is ignored.
  - Examples: "_a_/sset _b_/acked _s_/ecurity (abs)" defines "abs" as an
    acronym for "asset backed security" but "_a_/sset _b_/acked
    _s_/ecurity (beginner)" just ignore the parenthesis and do not
    create any potential abbreviation.

* __"Split Skills in Tokens"__
  * Description: Skills must be added on the platform as tokens (one
    skill after another). However, some people merged all skills in one
    token, this step allows to split them back. It uses identified
    splitter items (comma, pipe, semicolon, etc.) to recreate skills.
    This steps also split merged skills like "a/b" into 2 skill "a" and
    "b" (it allows to reduce the vocabulary size).
  * Examples: "python, machine learning" will be split into 2 skills
    "python" and "machine learning" for that user. Same "bash/shell
    script" will be split into 2 skills "bash script" and "shell
    script".

- __"Count Occurrences"__
  - Description: Count the number of appearances of each skill in its
    raw version (after splitting).

* __"Basic Cleaning"__
  * Description: Simple basic steps of text cleaning. Lowercase, remove
    unnecessary spaces replace some specific character ("&", "-") and
    few hand-selected words ("datum" -> "data", "medium" -> "media"). It
    also removes parentheses and their contents from skills.

- __"Check Presence in Referential"__
  - Description: Compare the "clean" version (after basic cleaning) of
    skills in both referential and JOGL extract. All matching skills
    will not be treated further and their count is updated in the
    referential.

* __"Check Occurrences over Threshold"__
  * Description: Split skills into 2 main cases. On one side, skills
    above the defined threshold (which are quite trusted for their
    value). On the other side, skills under (which are not trusted as
    references for skills (typos, etc.)).

- __Language identification (over Threshold)"__
  - Step 1/2 - Find software, tools, languages, etc.:
    - Description: This step treats skills that are containing numbers
      or skills that are partially/fully uppercased. A quick overview of
      skills with this pattern shows that these skills are almost all
      referring to a technology which do not have to be translated (SQL,
      A-110, JavaScript, etc.). To be considered as a _technology
      skill_, the skill must contain at least 1 number and/or at least
      as many uppercase characters than distinct words.
    - Examples: "JavaScript" contains 2 uppercase characters for only
      one word, then it's considered as a _technology skill_ and do not
      have to be translated. However, "Data management" which contains
      only 1 uppercase characters for 2 words will not be considered as
      a _technology skill_.
  - Step 2/2 - Identify the language of skills:
    - Description: There was no open-source package or pretrained model
     able to perform a language detection and translation. Then we used
     a __fastText pre-trained model__ to do the identification part. The
     model in use is in the "models" folder of this project (under
     `jogl/nlp`). For a given string to identify the model return a list
     of potential languages and a confidence estimator. Since the list
     is ordered by decreasing confidence level, we are taking the first
     one and comparing the confidence value associated to a manually
     defined threshold (see more in __To do/To improve__ section).
    - Example: If "Bonjour" is identified as French with a confidence
        level superior to 0.5 (which is our default threshold) then the
        language is set at "fr" for French. Otherwise, if "Kenavo" is
        identified as French with a confidence level superior of 0.1
        (which is under our default threshold of 0.5) then the language
        is set at "unk" for unknown.

* __"English Skills (over Threshold)"__
  * Description: Skills identified as expressed in English are added
    directly to the referential. Same for skills which are considered as
    a _technology skill_.

- __"French, Spanish and German Skills (over Threshold)"__
  - Description: Skills identified as expressed in French, Spanish or
    German are set apart for translation. Others (unknown language) will
    be treated differently.

* __"Language Translation (over Threshold)"__
  * Description: There was no open-source package or pretrained model
    able to perform a language detection and translation. Then we used
    few __Marian pre-trained transformers (from Huggingface)__ to do the
    translation part. After a quick analysis of skills languages, we
    identified mostly 3 foreign languages to translate: French, German
    and Spanish. In the dedicated function, 2 Marian transformers are
    loaded (1 for German, 1 for French & Spanish) and used on skills.
    However, there is one condition to allow the translation: the
    translation the foreign skill must exist in the English skills
    already present (it can be a technology based on a foreign language,
    see examples).
  * Examples: "Gestion de projets" is identified as French in step 2 and
    will be translated to "Project Management" since this English skill
    is already present in the list of skills. "Kubernetes" is identified
    as Estonian in step 2 and (if we were also dealing with the Estonian
    language) it would be translated as "Governors" but since the skill
    "Governors" is not present in the list, it's not translated.

- __"Typo Search (over Threshold)"__
  - Description: Search for unknown skills which have a close match in
    the referential (updated up to this point). The comparison is made
    with a simple Levenshtein distance of 1 for skills with at least 7
    characters.
  - Examples: "machin learning" will be identified as a typo of "machine
    learning" which is present in the referential.

* __"Close Match Found (over Threshold)"__
  * Description: Match found will be added as raw version of matches.
    Other will be added as new skills. Then "machin learning" will be
    added to referential a raw version for the clean version "machine
    learning".

- __"Typo Search (under Threshold)"__
  - Description: Search for unknown skills which have a close match in
    the referential (updated up to this point). The comparison is made
    with a simple Levenshtein distance of 1 for skills with at least 7
    characters.
  - Examples: "machin learning" will be identified as a typo
    of "machine learning" which is present in the referential.

* __"Close Match Found (under Threshold)"__
  * Description: Match found will be added as raw version of matches.
    Other will be added as new skills. Then "machin learning" will be
    added to referential a raw version for the clean version "machine
    learning".

- __"Regroup @ one space"__
  - Description: Compare all skills in the referential (under
    _enrich\_skill_ version) with themselves. Match skills which are
    only 1 or 2 spaces away to make them have the same _enrich\_skill_.
  - Examples: "data mining" and "datamining" are the same skills at 1
    space close. Then only one version of the clean skill will be kept
    ("data mining").

* __"Stemming (left skills)"__
	* Description: Perform a stemming transformation of skills to keep the root of skills. In preparation of a matching on the stemming form.
  * Examples: "manager" and "management" will have the same root "manag"
    and then will be identified as the same skill.

- __"Identify acronyms by context"__
  - Main description: Some tools acronyms are as famous as the full name
    of the tool and people can use either the full name or the acronym
    without precising the other one. It's for example the case for
    "AWS","AI", "IoT", etc. Here is the process to associate full skills
    to these famous abbreviations.
    - Step 1/4 - Find potential acronyms:
      - Description: The idea behind acronyms or abbreviation is to
        drastically reduce the number of characters used, most of the
        time we use 2 or 3 characters for acronyms, then we focused on
        short skills containing 2 or 3 characters only.
      - Examples: "AWS", "IoT" or "AI" are valid values for acronym
    - Step 2/4 - Apply current referential to skills
      - Description: So far, even if this process is running for the
        first time, there is an existing referential with some mappings
        produced by previous treatment bricks (or, even better, previous
        runs). Then it's possible to apply a referential to unify at
        least some skills. This step allows to reduce the total
        vocabulary size and then increase similarity of list of skills.
      - Examples: At the beginning of the process tasks like lowercasing
        or stripping are performed. If a user used the term "Python" and
        another one used "python " both skills will be replaced by
        "python". It was not possible before this replacement to detect
        that both users had the same skill "python".
    - Step 3/4 - Find potential meanings for acronyms
      - Description: For each potential acronym found in step 1/4, this
        step finds all full-length skills than can be the full version
        of the acronym (the rule of a potential meaning for an acronym
        is the same as previously presented).
      - Examples: For a given potential acronym "AI" this step could
        match "artificial intelligence", "autodesk inventor", "animal
        intelligence", etc...
    - Step 4/4 - Use contexts at user/project level to find the full
      skill
      - Description: Based on the list of possible meanings for an
        acronym -
        - Concatenate all skills associated with the acronym.
        - Concatenate all skills associated with the studied meaning.
        - Compare and measure the overlapping of skills between the 2
          contexts
        - Keep the most overlapped contexts as acronym-meaning
          pair.
      - Example: For the same meanings for "AI" if
        - "AI" is associated to "Data Science, Python, Computer Science"
        - "Artificial Intelligence" is associated to "Data Science,
            Computer Science, Python, Machine Learning, Python" -
            "Autodesk Inventor" is associated to "Python, Solidworks"
        - "Animal Intelligence" is associated to "Health Care,
          Sociology, Life Science" The skills overlapping between "AI"
          and "Artificial Intelligence" is 100% (on "AI" side). The
          skills overlapping between "AI" and "Autodesk Inventor" is 50%
          (on "Autodesk Inventor" side). The skills overlapping between
          "AI" and "Animal Intelligence" is 0% (on both side). Then the
          associated meaning is "Artificial Intelligence" (100% > 50% >
          0%)

* __"Stemming"__
  * Description: Perform a stemming transformation of skills in the
    _skills\_referential_ to store the root of skills.
  * Examples: "manager" and "management" will have the same root
      "manag" and then will be identified as the same skill.

- __"Check Presence in Referential (stem)"__
  - Description: Final try to match left skills using their stem form.
    If any stem form match a stem form of a skill in the referential
    then it is added a new version of the known skill.
  - Example: "crazy manager" is not matched yet and "crazy management"
    is in the referential. Both skills have the same root "crazi manag",
    then will be identified as the same skill and "crazy manager" will
    be a version of the skill "crazy management".



#### Part 3: Applying

This part is just applying referentials created and updated with parts 1 and 2. So far, it is used to improve the matching of users’ skills and projects needs in the emailing campaign. Skills needs to be unified before being targeted by the recommender system. 

* __Split Skills in Tokens__:
  * Description: Skills must be added on the platform as tokens (one
    skill after another). However, some people merged all skills in one
    token, this step allows to split them back. It uses identified
    splitter items (comma, pipe, semicolon, etc.) to recreate skills.
    This steps also split merged skills like "a/b" into 2 skill "a" and
    "b" (it allows to reduce the vocabulary size).
  * Examples: "python, machine learning" will be split into 2 skills
    "python" and "machine learning" for that user. Same "bash/shell
    script" will be split into 2 skills "bash script" and "shell
    script".

- __Apply Referential__:
  - Description: Use referential to map raw skills (after being
    tokenized) to clean ones present in the referential. Of course, only
    learnt skills present in the referential can be mapped


* __Basic Cleaning__:
  * Description: Simple basic steps of text cleaning. Lowercase, remove
    unnecessary spaces replace some specific character ("&", "-") and
    few hand-selected words ("datum" -> "data", "medium" -> "media")


- __Create unique IDs__:
  - Description: Each skill (present in JOGL database) has a specific
    ID. However, once there are clean, a clean skill may have several ID
    associated from all previous raw versions. This step recreates new
    ID unique for each clean skill. One important point to mention: This
    unique ID of clean skill IS NOT FIXED. Each time this predict part
    is apply, it generates a new unique ID for each skill. This ID will
    be used only by the recommender system.
  - Examples: "python " has the ID 3 and "Python" has the ID 124. The
    clean skill "python" can be associated to 3 and 124. Creating a new
    unique ID for "python" solve this issue.

* __Replace IDs__:
  * Description: The recommender system uses skills IDs at users and
    projects level. So, the creation of IDs at skills level can be apply
    on the new data (called network). Applying the mapping "Old ID",
    "New ID" allows to create new column which is used by the
    recommender as the reference index. One important thing to notice is
    that the code of the recommender system had to be changed to use
    this new column of new indexes.


#### Part 4: Algolia cleaning

__NOT IMPLEMENTED YET__

With the creation of a cleaned skill, it's now possible to remove all
different version of skill in the auto-completion of free typing field
for skills by only using the clean version or the most popular version
(like "Python" for all version of "python", "python ", "pyhton", etc.)


## Decisions and constraints 

* __Translation and language detection__

    2 kinds of models are used in this process. One for the language
    detection (fastText) and one for translation from an identified
    language (Marian MT from Huggingface). It has been done this way
    because no model / free and unlimited API was able to do both when
    we created this process.


## To do / To improve

* __Language identification__: 
  * Most of packages or models used for language identification return a
    confidence value with the language estimation to indicated some kind
    of sureness of the result provided. We did not have time to perform
    an analysis of the optimum value of threshold. What is important is
    to be quite sure of the estimation since the goal of the NLP part is
    to clean up the skill. Then it should be interesting to find the
    threshold which maximize the recall under the constraint of 95%
    precision (maybe 90% ?).
  * Implement an automatic report/return with statistics on languages
      identification to identify new languages to take into account if
      the volume of one language explodes (deployment in a new country,
      etc.).

- __Language translation__:
  -   Check if there is a possibility to have several translations for a
      given foreign word and match one of them to English skill. Like
      the French skill "Impression" which its first translation in
      English is "Impression" instead of "Print" or "Printing". The
      translation is technically correct but English speakers do not use
      "Impression", the use "Print" link in the skill "3D printing".
  - Finding a model which can translation all/lot of languages could be
   nice too, to not deal with manual research/implementation of new
   Marian Transformers.

* __"Identify acronyms by context"__:
  * The current process works at global level and define one meaning for
    an acronym. However different fields may have same acronyms for
    different tools (Example : "AI" could really work for "Artificial
    Intelligence" and "Autodesk Inventor" if the user is a data
    scientist or a designer). We defined it at global level because the
    volume of users/skills was not big enough to ensure stables results.
    Later it will be possible to make it work at user or project level.
  * Check if it's possible to deal with local language acronyms like
    "IA" in French for "Intelligence Artificielle". Maybe use the
    country/location of the user to determine if the language have to be
    switched.
