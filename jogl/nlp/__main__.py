import click

from jogl.nlp.stores import CSVStore, DatabaseStore
from jogl.nlp import engine

import os, sys
from os import getenv

@click.command()
@click.option('-i', '--init', is_flag=True, help='Type of job to launch.')
@click.option('-l', '--learn', is_flag=True, help='Type of job to launch.')
@click.option('-a', '--apply', is_flag=True, help='Type of job to launch.')
@click.option('-s', '--store', type=click.Choice(['csv', 'database']), default='database', help='Sets the type of source to get data from. Default: database')
@click.option('--input_skills_referential_csv', type=click.Path(exists=False), required=False, help='Input skills CSV file if the `--source csv` flag is present')
@click.option('--input_acronyms_referential_csv', type=click.Path(exists=False), required=False, help='Output skills CSV file if the `--target csv` flag is present')
@click.option('--input_skills_csv', type=click.Path(exists=True), required=False, help='Input skills CSV file if the `--source csv` flag is present')
@click.option('--input_network_csv', type=click.Path(exists=True), required=False, help='Input network CSV file if the `--source csv` flag is present')
# TODO NEED TO WRITE DOC FOR AWS

def main(init, learn, apply, store, input_skills_referential_csv, input_acronyms_referential_csv, input_skills_csv, input_network_csv):
    """
    JOGL Text Cleaning

        Need a job specification:
        'init' to initialize a full process
        'learn' to learn or relearn new association for cleaning
        'apply' to apply the mapping on current skills in users data

        Need an output specification:
        path to write a json output file.

        Learn more at <doc link to add>
    """
    if store == "csv":
        if input_skills_csv and input_acronyms_referential_csv and input_skills_referential_csv and input_network_csv:
            store = CSVStore(input_skills_csv, input_network_csv,
                             input_skills_referential_csv, input_acronyms_referential_csv)
        else:
            click.echo("ERROR: Missing CSV files path: input_skills_referential_csv or input_skills/network_csv or input_acronyms_referential_csv")
            sys.exit(1)

    elif store == "database":
        store = DatabaseStore()

    else:
        click.echo("ERROR: the store type you chose is non existant")
        sys.exit(1)

    # Initialize an NLP Object
    NLP_engine = engine.NLPEngine(store)
    # NLP_engine.initialize_referential()
    # NLP_engine.learn(X_train, None)
    # NLP_engine.apply(X_test)

    if init:
        # Preprocess LinkedIn
        click.echo('Initialisation process. Erasing all previously learned information')
        click.echo('Initialisation process: Running')
        #wf.wrap_initialize_referential(store)  # old wrap system
        NLP_engine.initialize_referential()
        click.echo('Initialisation process: Done')

    if init or learn:
        # Update mapping file from JOGL skills
        # click.echo('Collecting JOGL Data: Running')
        # jogl_data = nf.get_jogl_data(table="users")
        # click.echo('Collecting JOGL Data: Done')
        if init:
            click.echo('Post initializing learning step')
        click.echo('Learning : Running')
        #wf.wrap_learning_referential(store)  # old wrap system
        NLP_engine.fit()
        click.echo('Learning : Done')

    elif apply:
        # click.echo('Collecting JOGL Data: Running')
        # jogl_data = nf.get_jogl_data(table="users")
        # click.echo('Collecting JOGL Data: Done')

        click.echo('Applying: Running')
        #wf.wrap_applying_referential(store)  # old wrap system
        NLP_engine.predict()
        click.echo('Applying: Done')

    else:
        click.echo('Please use "init", "learn" or "apply". Learn more at <Link_to_doc>')
        sys.exit(1)


if __name__ == '__main__':
    main()
