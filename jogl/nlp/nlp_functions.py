import pandas as pd
import numpy as np

from re import sub, split, findall, match, search
from collections import Iterable

import fasttext
#model = fasttext.load_model('./jogl/nlp/models/lid.176.ftz')


from transformers import MarianMTModel, MarianTokenizer
from nltk.stem import PorterStemmer

ps = PorterStemmer()

import Levenshtein as lev

import warnings
warnings.filterwarnings("ignore")

REPLACE_LS = [("datum", "data"), ("medium", "media")]


def rem_trim_lower_str(input_str, other_replace_ls):

    """
    Function to remove unnecessary characters like multiple spaces, hyphens, uppercase.
    Allows to replace some words and the symbol "&".

    Inputs:
        input_str: str, a string to clean
        other_replace_ls: list of tuples, list of words to replace and their meaning (word_to_replace, replacement)

    Output:
        str, cleaned string
    """

    # Replace special characters "&" and "-" by meaning or space
    input_str = str(input_str).replace("&", " and ")
    input_str = str(input_str).replace("-", " ")

    # Trim and lowercase strings
    input_str = str(input_str).strip().lower()

    # Replace manually-selected words. Note : This method replace as substrings not as words
    if other_replace_ls is not None:
        for word, replacement in other_replace_ls:
            input_str = str(input_str).replace(word, replacement)

    return input_str


def rem_parentheses(input_str):
    """
    Function to remove parentheses and their content.

    Inputs:
        input_str: str, a string to clean

    Output:
        str, cleaned string
    """

    input_str = sub(r'\([^)]*\)', '', input_str)  # remove parentheses & content

    return input_str.strip()


def apply_function_to_list(list_skills, func):
    """
    Function to apply any function to each items of a list. Allows to have a more readable code when working on
    skills of users.

    Inputs:
        input_str: list, a list which contains elements on which a function need to be apply
        func: function, a function to apply on elements

    Output:
        list, list with function applied to elements
    """
    list_skills_lang = [func(x) for x in list_skills]

    return list_skills_lang


def split_enumeration(input_str):
    """
    Function to split multiples skills listed in 1 'token' instead of multiples ones.
    It searches for natural pipes in skills like " / \ | , ; - " to split the string by these pipes.
    It return a list of skills, even in the case of an untouched skill (list of size 1).
    TO DO :
        Maybe add a notion of length of the string to be sure it's an enumeration of skills

    Inputs:
        input_str: str, a string to clean

    Output:
        list, list of skill(s)

    """

    if not type(input_str) == str:
        input_str = ""

    input_str = sub(r'\([^)]*\)', '', input_str)  # remove parentheses

    # Looking for and spliting | , ; without spaces since these pipes can't be really use in skills
    if len(findall(r'[;,\|]', input_str)) > 0:
        list_subskills = split(r';|,|\|', input_str)
        list_subskills = [x.strip() for x in list_subskills]
        return list_subskills

    # Looking for and spliting / - \ (with spaces before or after)
    if len(findall(r' *[-/\\] ', input_str)) + len(findall(r' [-/\\] *', input_str)) > 0:
        # Keep information of space for theses skills: CI/CD, full-stack, etc.
        list_subskills = split(r'/ | /|- | -|\\ | \\', input_str)
        list_subskills = [x.strip() for x in list_subskills]
        return list_subskills

    else:
        return [input_str]


def recreate_full_subskill(input_str):
    """
    Function to clean multiples skills around a "/" character.
    According to the position of words around a "/" the meaning is different. There is the 4 following cases:

        Case 1: "word1 word2/word3 word4" <-> "word1 word2", "word3 word4". Example : "machine learning/deep learning"
        Case 2  "word1 word2/word3" <-> "word1 word2", "word1 word3". Example : "User Interface/Experience"
        Case 3: "word1/word2 word3" <-> "word1 word3", "word2 word3". Example : "Perl/Shell Scripting"
        Case 4: "word2/word3" <-> "word2", "word3". Example : "CI/CD"

    Inputs:
        input_str: str, a string to clean

    Output:
        list, list of skill(s) if a modification occurred
        str, unmodified input_string

    """

    input_str = sub(r'\([^)]*\)', '', input_str)
    # Case w1 w2/w3 w4 -> w1 w2, w3 w4
    if len(findall('[a-zA-Z0-9]+ [a-zA-Z0-9]+/[a-zA-Z0-9]+ [a-zA-Z0-9]+', input_str)) >= 1:
        # Split
        return input_str.split("/")

    # Case w1/w2 w3 -> w1 w3, w2 w3
    elif len(findall('[a-zA-Z0-9]+/[a-zA-Z0-9]+ [a-zA-Z0-9]+', input_str)) >= 1:
        word_to_copy = input_str.split(" ")[1]
        second_split = input_str.split(" ")[0].split("/")
        return [str(x) + " " + str(word_to_copy) for x in second_split]

    # Case w1 w2/w3 -> w1 w2, w1 w3
    elif len(findall('[a-zA-Z0-9]+ [a-zA-Z0-9]+/[a-zA-Z0-9]+', input_str)) >= 1:
        word_to_copy = input_str.split(" ")[0]
        second_split = input_str.split(" ")[1].split("/")
        return [str(word_to_copy) + " " + str(x) for x in second_split]

    # Case w1/w2 -> w1, w2
    elif len(findall('[a-zA-Z0-9]+/[a-zA-Z0-9]+', input_str)) >= 1:
        return input_str.split("/")

    else:
        return input_str


def flatten(nested_list):
    """
    Function to transform nested lists in a main list to a 1-level list.
    Needed after the functions "recreate_full_subskill" and "split_enumeration".

    Inputs:
        list: nested lists

    Output:
        list, 1-level list

    """

    for elt in nested_list:
        if isinstance(elt, Iterable) and not isinstance(elt, (str, bytes)):
            yield from flatten(elt)
        else:
            yield elt


def check_alpha_character(input_str):
    """
    Function to check if there is any alpha character in the string.
    Allow to prevent some issues later due to no alpha character in strings.

    Inputs:
        input_str: str, a string to check

    Output:
        bool, True if there is at least 1 alpha character otherwise False

    """

    try:
        return input_str.upper().isupper()
    except AttributeError:
        return False


def is_acronym_specified(abbrev_str, text_str):
    """
    Function to check if the content of parentheses might be an acronym of the text outside of parentheses.
    Basically check if each letter of the potential acronym is the first letter of each word (or same order).

    Example: "gcp" matches "google cloud platform"

    Inputs:
        abbrev_str: str, a string that can be an acronym of "text_str"
        text_str: str, a string that can be the extended form of the acronym "abbrev_str"

    Output:
        bool, True if the regular expression identifies an acronym

    """

    pattern = "(|.*)".join(abbrev_str.lower())
    return match("^" + pattern, text_str.lower()) is not None


def language_detect_ft(input_string, model, threshold=0.5):
    """
    Function to detect language of a string using the fasttext package. Estimation of the language is accepted if
    the minimum confidence probability of the estimate reached a manually defined threshold.
    If the confidence threshold is not reached then the language is considered as unknown 'unk'.

    Inputs:
        input_str: str, a string to estimate
        threshold: float (0. to 1.), minimum confidence probability to be reached by the attribute "confidence" of the
                    googletrans detection.

    Output:
        bool, True if there is at least 1 alpha character otherwise False

    """
    # Estimate language
    est = model.predict(input_string, k=1)
    lang = est[0][0]
    conf = est[1][0]

    if conf >= threshold:
        out = sub('__label__', '', lang)
        return out

    else:
        return 'unk'


def load_mar_models():
    """
    Function to import models used for translation. So far it allows to translate 'de' (german),
    'fr' (french) and 'es' (spanish) to english. Will have to ba change manually to add new languages.
    Here is the official documentation for MarianMT transformers :
    https://huggingface.co/transformers/model_doc/marian.html
    """
    # German model
    tokenizer_de = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-de-en')
    model_de = MarianMTModel.from_pretrained('Helsinki-NLP/opus-mt-de-en')

    # French & Spanish
    tokenizer_ro = MarianTokenizer.from_pretrained('Helsinki-NLP/opus-mt-ROMANCE-en')
    model_ro = MarianMTModel.from_pretrained('Helsinki-NLP/opus-mt-ROMANCE-en')

    to_dict = {'de': (tokenizer_de, model_de),
               'fr': (tokenizer_ro, model_ro),
               'es': (tokenizer_ro, model_ro)}

    return to_dict


def language_transl_hf(input_str, input_lan, dict_lan_model, check_presence=None):
    """
    Function to translate a string to english using a Marian transformer (from https://huggingface.co).
    It uses a source language (input_lan) and currently cover the following languages: 'de', 'fr' and 'es'.
    "check_presence" should be a list of translated terms (used as reference) to not translate terms
    if their translation does not appear in the list.

    Note:
        A while loop is needed since the package may have some troubles to succeed every time.

    Inputs:
        input_str: str, a string to estimate
        input_lan: str, language of the string
        dict_lan_model: dict, predifined association language-model.
        check_presence: str, list of translated reference terms.

    Output:
        str, translated (or not) string

    """

    # From language, get correct model and tokenizer
    tokenizer_, model_ = dict_lan_model.get(input_lan, ('', ''))

    if tokenizer_ == '' or model_ == '':
        print('Language not supported for "{}", as "{}".'.format(input_str, input_lan))
        return input_str

    # Transform string for transfomer
    reshape_input_string = ['>>eng<< ' + str(input_str)]

    translated = model_.generate(**tokenizer_.prepare_seq2seq_batch(reshape_input_string, return_tensors="pt"))
    tgt_text = [tokenizer_.decode(t, skip_special_tokens=True) for t in translated]

    if len(tgt_text) > 0:
        out_str = tgt_text[0].lower()
        out_str = sub(r'[^\w\s]', '', out_str)
        out_str = out_str.strip()

        if check_presence is not None and isinstance(check_presence, list):
            if out_str not in check_presence:
                out_str = input_str
                return out_str  # equals to input_string

            else:
                return out_str  # equals to translator.translate

        else:
            return out_str  # equals to translator.translate

    else:
        print('Translation not found for "{}", as "{}".'.format(input_str, input_lan))
        return input_str


def language_transl_lg_old(input_string, input_lan, check_presence=None):
    """
    Function to translate a string to english using the deep-translator package (Linguee).
    It can perform an automatic language detection or use a source language (input_lan).
    Threshold is the minimum confidence probability of the estimate to accept it as the language (for 'auto' only).
    check_presence should be a list of translated reference terms to not translate terms
    if their translation does not appear in the list.

    Note:
        A while loop is needed since the package may have some troubles to succeed every time.

    Inputs:
        input_str: str, a string to estimate
        input_lan: str, language of the string (if known) otherwise 'auto'
        check_presence: str, list of translated reference terms.

    Output:
        str, translated (or not) string

    """
    try:

        time.sleep(2)
        if input_lan != 'en' and sum(c.isalpha() for c in input_string) > 4:
            print(input_string)
            translated = LingueeTranslator(source=input_lan, target='english').translate(input_string)
            out = translated.strip()  # .split('/')[0]

            if check_presence is not None and isinstance(check_presence, list):
                if out not in check_presence:
                    out = input_string

                    return out  # equals to input_string

                else:
                    return out  # equals to translator.translate

            else:
                return out  # equals to translator.translate

        else:
            return input_string  # equals to input_string (english or short word)

    except ElementNotFoundInGetRequest:
        print("Issue in translating: " + str(input_string))
        return input_string

    except RequestError:
        print("Error with request, probably banned from Linguee")
        return input_string


def match_upper_number(input_string, language):
    """
    Function to identify skills which are not from a specific language (technology, script language, tools, etc.)
    Check for presence of numbers (k8s, A-110, etc.), partially or fully uppercased (JavaScript, AAS, etc.)

    Any skills matching these rules is associated with the English language so that they are not translated at a
    later step.

    Inputs:
        input_str: str, a string to estimate
        language: str, identified language of the string

    Output:
        str, language of the string

    """

    match = search(r'\d', input_string)
    upper_count = sum(1 for c in input_string if c.isupper())
    words = len(findall(r"[\w']+", input_string))

    if match or upper_count >= words or input_string == input_string.upper():
        return 'en'

    else:
        return language


def get_typos(input_str, referential_list):
    """
    Function to find a close skill to a given string. It's based on the Levenshtein distance.
    Only distance of 1 are take into account for words with at least 6 letters.

    Inputs:
        input_str: str, a string possibly close to an existing one
        referential_list: list, list of referenced strings

    Output:
        str, the closest string found or '' if nothing was found

    """

    list_close = [x for x in list(set(referential_list))
                  if lev.distance(input_str, x) <= 1 and len(input_str) > 6]

    if len(list_close) > 0:
        return list_close[0]

    else:
        return ''


def just_a_space(input_str1, input_str2):
    """
    Function to compare if two strings have same characters modulo spaces.
    Return both strings with the shortest first if they identical (modulo spaces).

    Inputs:
        input_str1: str, first string to compare
        input_str2: str, second string to compare

    Output:
        tuple, both input string if they are identical, '' otherwise

    """

    if ''.join(input_str1.split()) == ''.join(input_str2.split()):
        if len(input_str1) > len(input_str2):
            return input_str2, input_str1
        else:
            return input_str1, input_str2
    else:
        return ''


def is_one_space_away(list_skills):
    """
    Function to iterate over a list of string to find similar ones (modulo space).
    Test strings up to 2 spaces.

    Inputs:
        list_skills: list, a list of strings

    Output:
        list, list of tuple of similar skills (shortest first)

    """

    one_space_away = []
    for x in list_skills:
        for y in list_skills:
            if lev.distance(x, y) <= 2:
                out = just_a_space(x, y)

                if out != '':
                    one_space_away.append(out)

    one_space_away = list(set(one_space_away))

    return one_space_away


def format2referential(list_clean, raw_clean_df, col_raw='raw_skill', col_clean='clean_skill'):
    """
    Function to transform a list of skills to a dataframe shaped like the referential.

    Inputs:
        list_clean: list, list of skills to reshape
        raw_clean_df: dataframe, initial dataframe of skills
        col_raw: str, column which contains raw skills
        col_clean: str, column which contains cleaned skills

    Output:
        dataframe, referential-shaped dataframe ready to be added

    """

    # Filter  df_user with the list of skills to keep
    raw_clean_df = raw_clean_df[raw_clean_df[col_clean].isin(list_clean)]

    # Count skills based on raw form & cleaned form ('python' -> 'python' et ' Python' -> 'python')
    count_raw_clean_df = raw_clean_df.groupby([col_raw, col_clean]).sum().reset_index()

    # Duplicate 'translate' column
    count_raw_clean_df.columns = ['raw_skill', 'clean_skill', 'flag']
    count_raw_clean_df['enrich_skill'] = count_raw_clean_df['clean_skill']
    count_raw_clean_df['isinitref'] = 0

    return count_raw_clean_df


def stemming_nltk(input_str):
    """
    Function to stem all words in a string.

    Inputs:
        input_str: str, string to stem

    Output:
        str, stemmed string

    """

    try:

        # exceptions = ['it', 'data', 'media']
        # Extract the lemma for each token and join. Deal with exceptions
        out = " ".join([ps.stem(w) if len(w) > 3 else w for w in input_str.split()])

        # Remove multiple spaces (just in case)
        out = ' '.join(out.split())

        # Remove spaces at the end or begining (due to cleaned tokens and just in case)
        out = out.strip().lower()

        # Case its acronyms if empty after stemming
        if out == '':
            out = input_str
        return out

    except:
        # print('error with: ' + str(input_str))
        return input_str


# --- WORKING WITH ACRONYMS --- #
def can_be_acronym(acron_str, test_str):
    """
    Since most of acronyms are based on first letter of each word
        Ex. ui = 'user interface'
            gcp = 'google cloud platform'
            ai = 'artificial intelligence'
            etc.

    This function checks if 1st letters of each words can match a potential acronym.

    Inputs:
        acron_str: str, acronym string
        test_str: str, full lentgh string


    Output:
        bool, True if 1st letters of each words match the acronym string.

    """

    return "".join(findall(r"\b(\w)", test_str)) == acron_str


def list_matching_acron_texts(acron_str, list_text):
    """
    Function to list all possible meaning (from a given list) of an acronym.

    Inputs:
        acron_str: str, acronym string
        list_text: list, list of string

    Output:
        list, list of elements that match the condition of the function "can_be_acronym" on acron_str

    """

    return [text_str for text_str in list_text if can_be_acronym(acron_str, text_str.lower()) is True]


def average_context(list_skills, df_user, col_df_skills):
    """
    Function to list all skills associated to a given one. It's cumulated over each user.
    Will be used to estimate the context of a skill.

    Inputs:
        list_skills: list, list of skills for which a context is needed.
        df_user: dataframe, dataframe with skills by user.
        col_df_skills: str, column of the previous dataframe which contains skills.

    Output:
        list, tuple of a skill and it's context

    """

    contexts = []
    for x in list_skills:
        # Get list of skills from the users dataframe according to the presence of x
        # ct = list(df_user[pd.DataFrame(df_user[col_df_skills].tolist()).isin([x]).any(1)][col_df_skills])
        ct = list(df_user[df_user[col_df_skills].apply(lambda skill: x in skill)][col_df_skills])

        # Flatten result
        ct_flat = [item for sublist in ct for item in sublist if item not in [x]]

        # Don't add empty contexts, it's useless and time consuming
        if len(ct_flat) > 0:
            contexts.append((x, ct_flat))

    return contexts


def distance_lists(list1, list2):
    """
        Function to compute the overlap ratio between two list. Probably really close to Hamming distance.
        Since the ratio is based on the length of lists.
        It's based on the maximum ratio overlap between both permutations

        Inputs:
            list1: list, first list to compare
            list2: list, second list to compare

        Output:
            float, max ratio found

        """

    score1 = np.mean([1 if i in list2 else 0 for i in list1])
    score2 = np.mean([1 if i in list1 else 0 for i in list2])

    return max(score1, score2)


def most_likely_meaning(acron_str, acron_context, contexts):
    """
    Function to estimate the maximum likelihood of a acronym to have a certain meaning.

    Note: Research is based overall and not at user level since volume of users/ skills does not allow it.

    Inputs:
        acron_str: str, acronym which have to be estimated
        acron_context: list, acronym context in skills
        contexts: list, tuples of potential meanings and their resp. contexts

    Output:
        tuple, acronym, it's meaning and the ratio

    """

    # Compare context of acronym to contexts of potential meanings
    closeness = [(x[0], distance_lists(x[1], acron_context)) for x in contexts]

    # Sort by similarity (increasing)
    closeness.sort(key=lambda x: x[1])

    # Get maximum similarity (last one)
    meaning = closeness[-1][0]

    return acron_str, meaning, closeness[-1][1]


# --- APPLY REFERENTIAL CLEANING TO SKILLS --- #
def apply_referential(df_skill, col_skill,
                      df_referential, col_ref_from, col_ref_to,
                      filter_exist=True):
    """
    Function to clean skills of users based on clean skills in the referential.

    Inputs:
        df_skill: dataframe, user base with skills
        col_skill: str, column of "df_skill" which contains skills
        -- col_skill_out: str, column of "df_skill" which will contain cleaned skills
        df_referential: dataframe, referential of skills
        col_ref_from: str, column of "df_referential" which contains skills common with "df_skill"
        col_ref_to: str, column of "df_referential" which contains cleaned skills
        filter_exist: bool, set to True only used existing skills (and not all referential)

    Output:
        dataframe, df_skill with a new column of cleaned skills
    """

    # Used to lighten process
    if filter_exist is True:
        df_referential = df_referential[df_referential.flag >= 1]  # Only existing skills

    # Filter on usefull columns. Drop potential duplicates
    df_referential_mapping = df_referential[[col_ref_from, col_ref_to]].drop_duplicates(keep='first',
                                                                                        subset=[col_ref_from])

    ### OLD
    # Zip column allowing to use it in a replace function
    #ls_ref_map = list(zip(df_referential_mapping[col_ref_from], df_referential_mapping[col_ref_to]))

    # Clean skills
    #df_skill[col_skill_out] = df_skill[col_skill].apply(lambda x: list(pd.Series(x).replace(dict(ls_ref_map))))

    ### NEW
    # Create a dict of mapping
    dict_ref_map = pd.Series(df_referential_mapping[col_ref_to].values,
                             index=df_referential_mapping[col_ref_from]).to_dict()

    # Clean skills
    df_skill = df_skill.replace({col_skill: dict_ref_map})

    return df_skill
