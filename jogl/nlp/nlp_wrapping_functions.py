import warnings
warnings.filterwarnings("ignore")

import jogl.nlp.nlp_functions as nf
import pandas as pd
import numpy as np
import re  # Regex Formulas
import fasttext

from collections import Counter

ft_model = fasttext.load_model('lid.176.ftz')

REPLACE_LS = [("datum", "data"), ("medium", "media")]


def wrap_initialize_referential(store, model=ft_model):
    """
    :param store:
    :return:

    ça c'est OK pour le moment
    """

    # Functions used
    f = open('./jogl/nlp/samples/initial_skills.txt', 'r')
    lkdn_skills_raw = f.read()
    # lkdn_skills_raw = nf.readfroms3(bucket_name, s3_prefix + 'all_skill_lkdn.txt')
    lkdn_skills = eval(lkdn_skills_raw)
    #lkdn_skills = lkdn_skills[:1000]

    # -------------------------------------- #
    # --- Create and clean the DataFrame --- #
    df_lkdn_ref = pd.DataFrame({'raw_skill': lkdn_skills})

    # Clean LinkedIn skills (only trim & lower)
    df_lkdn_ref['clean_skill'] = df_lkdn_ref['raw_skill'].apply(lambda x: nf.rem_trim_lower_str(x, REPLACE_LS))

    # ----------------------------------- #
    # ---  Get specific abbreviations --- #
    df_lkdn_parenth = pd.DataFrame(df_lkdn_ref[df_lkdn_ref.clean_skill.str.contains("\(")]['clean_skill'])

    df_lkdn_parenth['acronym'] = df_lkdn_parenth['clean_skill'].apply(lambda s: re.search(r'\((.*?)\)', s).group(1))

    # Get things outside of the parentheses
    df_lkdn_parenth['meaning'] = df_lkdn_parenth['clean_skill'].apply(lambda s: re.sub(r'\([^)]*\)', '', s))

    # Removed possible space left (when removing parentheses)
    df_lkdn_parenth['meaning'] = df_lkdn_parenth['meaning'].apply(lambda x: x.strip())
    df_lkdn_parenth['acronym'] = df_lkdn_parenth['acronym'].apply(lambda x: x.strip())

    df_lkdn_parenth = df_lkdn_parenth[~df_lkdn_parenth.acronym.str.contains("t use")]
    df_lkdn_parenth = df_lkdn_parenth[~df_lkdn_parenth.acronym.str.contains("deprecated")]
    # df_lkdn_parenth
    # Store in a DataFrame
    df_acronyms = df_lkdn_parenth[['meaning', 'acronym']].drop_duplicates()
    df_acronyms['flag'] = [0] * df_acronyms.shape[0]

    del df_lkdn_parenth

    # -------------------------------------- #
    # ---  Finish cleaning the DataFrame --- #

    # Remove parentheses details from referential
    df_lkdn_ref['clean_skill'] = df_lkdn_ref['clean_skill'].apply(lambda x: nf.rem_parentheses(x))

    # Remove acronyms tags from referential
    df_lkdn_ref = df_lkdn_ref[~df_lkdn_ref.clean_skill.isin(list(df_acronyms.acronym))]

    # Remove non-alpha characters and other issues
    df_lkdn_ref['check'] = df_lkdn_ref['clean_skill'].apply(lambda x: nf.check_alpha_character(x))
    df_lkdn_ref = df_lkdn_ref[df_lkdn_ref.check == True]
    df_lkdn_ref = df_lkdn_ref.drop(['check'], axis=1)

    # ------------------------------------- #
    # ---  Translate skills (if needed) --- #

    # Create a column for language of skills set by default as "unk" for unknown.
    df_lkdn_ref['lg_skill'] = "unk"

    # Filter skills to not translate (Tools, Proper noun, etc.). It is based on numbers and uppercase letters
    df_lkdn_ref['lg_skill'] = df_lkdn_ref.apply(lambda x: nf.match_upper_number(x['raw_skill'], x['lg_skill']), axis=1)

    # Remove skills which are not anymore "unk"
    df_unk = df_lkdn_ref[df_lkdn_ref.lg_skill == "unk"]
    df_wait = df_lkdn_ref[df_lkdn_ref.lg_skill != "unk"]

    # Perform language identification
    df_unk['lg_skill'] = df_unk['clean_skill'].apply(lambda x: nf.language_detect_ft(x, model=model))

    # Perform translations
    #After a quick human overview.
    # 'de', 'fr', 'es' have to be translated (more than 100 skills most of them are language specific)
    # languages with less than 10 skills are mostly tools and non-language specific (>99%)
    # languages with  10 to 100 skills (mostly around 20 skills) -> are mostly tools and non-language specific (>90%)
    # Unknown language can not been translated (no language source) but most of them seem to be Tools, Technos etc.

    to_translate = ['de', 'es', 'fr']
    df_not_translate = df_unk[~df_unk.lg_skill.isin(to_translate)]
    df_to_translate = df_unk[df_unk.lg_skill.isin(to_translate)]

    # Get transformer models to process languages
    dict_lg_models = nf.load_mar_models()

    df_to_translate['enrich_skill'] = df_to_translate.apply(lambda x:
                                                            nf.language_transl_hf(input_str=x['clean_skill'],
                                                                                  input_lan=x['lg_skill'],
                                                                                  dict_lan_model=dict_lg_models)
                                                            , axis=1)

    # Duplicate 'clean_skill' column as skills in it are supposed to be in english or tools to not translate.
    df_wait['enrich_skill'] = df_wait['clean_skill']
    df_not_translate['enrich_skill'] = df_not_translate['clean_skill']

    # Quick check
    if df_lkdn_ref.shape[0] != df_wait.shape[0] + df_not_translate.shape[0] + df_to_translate.shape[0]:
        print("Not same number of rows ?")

    # Merge back all parts of the referential
    df_lkdn_ref = pd.concat([df_wait, df_not_translate, df_to_translate])

    del df_wait, df_not_translate, df_to_translate, df_unk, to_translate

    # Stem skills
    df_lkdn_ref['stem_skill'] = df_lkdn_ref['enrich_skill'].apply(lambda x: nf.stemming_nltk(x))

    # ------------------------------------------ #
    # ---  Reshape tables in correct format  --- #

    # - Acronyms
    df_acronyms['declared'] = 1
    df_acronyms['proba'] = 0.2

    # - Skills
    # Add a flag column (will be frequency in learn phase)
    df_lkdn_ref['flag'] = 0
    df_lkdn_ref['isinitref'] = 1

    # --------------------------- #
    # ---  Write files in S3  --- #

    store.skills_referential = df_lkdn_ref
    store.acronyms_referential = df_acronyms

    store.save_skills_referential()
    store.save_acronyms_referential()


    # nf.backup_writes3(df=df_lkdn_ref, file_s3=s3_prefix + 'skills_referential.json', need_back_up=True,
    #                   bucket_name=bucket_name)
    #
    # nf.backup_writes3(df=df_acronyms, file_s3=s3_prefix + 'acronyms_referential.json', need_back_up=True,
    #                   bucket_name=bucket_name)


def wrap_learning_referential(store, threshold=3, model=ft_model):
    """

    :param store:
    :param threshold:
    :param model:
    :return:
    """
    # ------------------------ #
    # --- Get referentials & JOGL data--- #

    if len(store.acronyms_referential):
        acronyms_referential = store.acronyms_referential
    else:
        acronyms_referential = store.load_acronyms_referential()

    if len(store.skills_referential):
        skills_referential = store.skills_referential
    else:
        skills_referential = store.load_skills_referential()

    if len(store.skills):
        df_skills = store.skills
    else:
        df_skills = store.load_skills()

    if len(store.network):
        df_network = store.network
    else:
        df_network = store.load_network()

    # ------------------------------------------ #
    # --- Join JOGL Network and Skill tables --- #

    df_network = df_network[df_network.targetable_node_type == "Skill"]

    df_network = df_network.merge(df_skills, left_on=['targetable_node_id'], right_on=['id'], how='left')
    df_network = df_network[['sourceable_node_type', 'sourceable_node_id', 'targetable_node_type', 'skill_name']]

    df_network = df_network[df_network.skill_name.isna() == False]
    # ----------------------------------- #
    # --- Preprocess non-token skills --- #

    # Split multiple skills based on separators and reshape dataframe (1 skill per row)
    df_network['skill_name'] = df_network['skill_name'].apply(lambda x: nf.split_enumeration(x))
    df_network = df_network.explode('skill_name', ignore_index=True) # see https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.explode.html

    # Split share subpart or combination of skills and reshape dataframe (1 skill per row).
    # E.g "bash/shell script" -> "bash script", "shell script" or "ci/cd" -> "ci", "cd"
    df_network['skill_name'] = df_network['skill_name'].apply(lambda x: nf.recreate_full_subskill(x))
    df_network = df_network.explode('skill_name', ignore_index=True)

    # ---------------------------------- #
    # --- Preprocess skills cleaning --- #

    # List of all skills
    df_jogl_skills = pd.DataFrame(list(Counter(df_network['skill_name']).items()), columns=['raw_skill', 'flag'])

    # Soft cleaning of JOGL skills
    df_jogl_skills['clean_skill'] = df_jogl_skills['raw_skill'].apply(lambda x: nf.rem_trim_lower_str(x, REPLACE_LS))

    # Remove empty,  etc.
    df_jogl_skills = df_jogl_skills[df_jogl_skills.clean_skill.apply(lambda x: nf.check_alpha_character(x) == True)]

    # ------------------------------------ #
    # --- Search for declared acronyms --- #

    # - Extract acronyms (passer en fonction ce sera plus clean)
    all_skills_tmp = df_skills[df_skills.skill_name.isna() == False].skill_name.values

    df_jogl_tmp = pd.DataFrame({'work_skill': all_skills_tmp})

    # Clean JOGL skills (only trim & lower)
    df_jogl_tmp['work_skill'] = df_jogl_tmp['work_skill'].apply(lambda x: nf.rem_trim_lower_str(x, REPLACE_LS))

    # Create the abbreviation referential
    df_jogl_parenth = df_jogl_tmp[df_jogl_tmp.work_skill.str.contains('\(')]

    # Get things inside of the parentheses
    df_jogl_parenth['acronym'] = df_jogl_parenth['work_skill'].apply(lambda s: re.search(r'\((.*?)\)', s).group(1))

    # Get things outside of the parentheses
    df_jogl_parenth['meaning'] = df_jogl_parenth['work_skill'].apply(lambda s: re.sub(r'\([^)]*\)', '', s))

    # Define a condition on existence
    if len(df_jogl_parenth) > 0:
        # Check if it's an abbreviation
        df_jogl_parenth['isacronym'] = df_jogl_parenth.apply(lambda x: nf.is_acronym_specified(x.acronym, x.meaning), axis=1)

        # Keep abbreviations
        df_jogl_parenth = df_jogl_parenth[df_jogl_parenth.isacronym == True]

        # Removed possible space left (when removing parentheses)
        df_jogl_parenth['meaning'] = df_jogl_parenth['meaning'].apply(lambda x: x.strip())
        df_jogl_parenth['acronym'] = df_jogl_parenth['acronym'].apply(lambda x: x.strip())

        # Store in DataFrame
        df_acronyms = df_jogl_parenth[['meaning', 'acronym']].drop_duplicates()
        df_acronyms['declared'] = [1] * df_acronyms.shape[0]
        df_acronyms['proba'] = [0.2] * df_acronyms.shape[0]

        if len(df_acronyms) > 0:
            # Append new acronyms to referential
            acronyms_referential = acronyms_referential.append(df_acronyms)
            acronyms_referential = acronyms_referential.drop_duplicates(keep='first')

        del df_jogl_tmp, all_skills_tmp, df_jogl_parenth, df_acronyms

    else:
        del df_jogl_parenth, df_jogl_tmp, all_skills_tmp

    # ------------------------------------- #
    # --- Matchs with referenced skills --- #

    # - Match & unmatch JOGL skills (clean) to referential skills (clean)

    df_jogl_unmatch = df_jogl_skills[~df_jogl_skills.clean_skill.isin(list(skills_referential['clean_skill']))]
    df_jogl_match = df_jogl_skills[df_jogl_skills.clean_skill.isin(list(skills_referential['clean_skill']))]
    #

    df_unmatch_count = df_jogl_unmatch.groupby(by=["clean_skill"]).sum().reset_index()
    df_unmatch_count.columns = ['clean_skill', 'flag']

    df_unmatch_popular = df_unmatch_count[df_unmatch_count.flag >= threshold]

    df_unmatch_other = df_unmatch_count[df_unmatch_count.flag < threshold]

    del df_unmatch_count

    # ------------------------------ #
    # --- Get language of skills --- #

    df_unmatch_popular['lg_skill'] = df_unmatch_popular['clean_skill'].apply(lambda x: nf.language_detect_ft(x, model=model))

    df_popular_en = df_unmatch_popular[df_unmatch_popular['lg_skill'] == "en"]  # ajout direct
    df_popular_unk = df_unmatch_popular[df_unmatch_popular['lg_skill'] == "unk"]

    df_popular_lan = df_unmatch_popular.loc[~df_unmatch_popular['lg_skill'].isin(['en', 'unk'])]

    # ---------------------------------------- #
    # --- Add firsts skills to referential --- #

    df_en = nf.format2referential(list(df_popular_en.clean_skill), df_jogl_skills)  # FROM df_popular_en
    skills_referential = pd.concat([skills_referential, df_en])

    df_match = nf.format2referential(list(df_jogl_match.clean_skill), df_jogl_skills)  # FROM df_jogl_match
    skills_referential = pd.concat([skills_referential, df_match])

    del df_en, df_match, df_popular_en, df_jogl_match

    # -------------------------------------------- #
    # --- Add translated skills to referential --- #

    reference_skills = list(skills_referential.enrich_skill)

    # Get transformer models to process languages
    dict_lg_models = nf.load_mar_models()

    df_popular_lan['enrich_skill'] = df_popular_lan.apply(
                                            lambda x: nf.language_transl_hf(input_str=x['clean_skill'],
                                                                            input_lan=x['lg_skill'],
                                                                            dict_lan_model=dict_lg_models,
                                                                            check_presence=reference_skills), axis=1)

    # Prepare the correct format to insert in referential
    df_lan = nf.format2referential(list(df_popular_lan.clean_skill), df_jogl_skills)

    # Finalize the correct format by changing the enrich_skill by the translation
    df_popular_lan = pd.merge(df_lan[['raw_skill', 'clean_skill', 'flag', 'isinitref']],
                              df_popular_lan[['clean_skill', 'enrich_skill']],
                              on='clean_skill', how='left')

    # Add to referential
    skills_referential = pd.concat([skills_referential, df_popular_lan])

    # free some memory
    del reference_skills, df_lan, df_popular_lan

    # -------------------------------------- #
    # --- Correct typos : Popular skills --- #

    # - Get a referential
    existing_ref_df = skills_referential.sort_values(by='flag', ascending=False)
    existing_ref_df = existing_ref_df[existing_ref_df.flag >= 1]
    existing_ref_list = existing_ref_df.clean_skill

    # - Treat typos
    df_popular_unk['clean_found_skill'] = df_popular_unk['clean_skill'].apply(lambda x:
                                                                              nf.get_typos(x, existing_ref_list))

    try:
        # Get typos only
        df_popular_typos = df_popular_unk[df_popular_unk.clean_found_skill != '']

        # Reshape typos dataframe
        df_popular_typos = df_popular_typos[['clean_skill', 'clean_found_skill', 'flag']]  # should be distinct
        df_popular_typos.columns = ['typo_skill', 'clean_skill', 'flag']  # beware new name switched !

        # Prepare the correct format to insert in referential
        df_tmp_typos = nf.format2referential(list(df_popular_typos.typo_skill), df_jogl_skills)
        df_tmp_typos = df_tmp_typos.rename(columns={'clean_skill': 'typo_skill'})

        # Join typo raw_skill to correct clean_skill
        df_popular_typos = pd.merge(df_tmp_typos[['raw_skill', 'typo_skill', 'flag', 'isinitref']],
                                    df_popular_typos[['typo_skill', 'clean_skill']], on='typo_skill', how='left')

        # Keep columns to add in referential
        df_popular_typos = df_popular_typos[['raw_skill', 'clean_skill', 'flag', 'isinitref']]

        # Join correct enrich_skill to each clean_skill
        df_popular_typos = pd.merge(df_popular_typos, existing_ref_df[['clean_skill', 'enrich_skill']].drop_duplicates(),
                                    on='clean_skill', how='left')

        # - Add to referential
        skills_referential = pd.concat([skills_referential, df_popular_typos])

    except ValueError:
        # Means dataframe is empty
        pass

    # Get non-typos
    df_left = df_popular_unk[df_popular_unk.clean_found_skill == '']

    # Prepare the correct format to insert in referential
    df_popular_left = nf.format2referential(list(df_left.clean_skill), df_jogl_skills)

    # - Add both to referential
    skills_referential = pd.concat([skills_referential, df_popular_left])

    del df_tmp_typos, df_left, df_popular_typos, df_popular_left

    # -------------------------------------------- #
    # --- Correct typos : Low frequency skills --- #

    # - Treat typos
    df_unmatch_other['clean_found_skill'] = df_unmatch_other['clean_skill'].apply(lambda x:
                                                                                  nf.get_typos(x, existing_ref_list))

    # Get typos only
    df_low_freq_typos = df_unmatch_other[df_unmatch_other.clean_found_skill != '']

    # Reshape typos dataframe
    df_low_freq_typos = df_low_freq_typos[['clean_skill', 'clean_found_skill', 'flag']]  # should be distinct
    df_low_freq_typos.columns = ['typo_skill', 'clean_skill', 'flag']  # beware new name switched !

    # Prepare the correct format to insert in referential
    df_tmp_typos = nf.format2referential(list(df_low_freq_typos.typo_skill), df_jogl_skills)
    df_tmp_typos = df_tmp_typos.rename(columns={'clean_skill': 'typo_skill'})

    # Join typo raw_skill to correct clean_skill
    df_low_freq_typos = pd.merge(df_tmp_typos[['raw_skill', 'typo_skill', 'flag', 'isinitref']],
                                 df_low_freq_typos[['typo_skill', 'clean_skill']], on='typo_skill', how='left')

    # Keep columns to add in referential
    df_low_freq_typos = df_low_freq_typos[['raw_skill', 'clean_skill', 'flag', 'isinitref']]

    # Join correct enrich_skill to each clean_skill
    df_low_freq_typos = pd.merge(df_low_freq_typos, existing_ref_df[['clean_skill', 'enrich_skill']].drop_duplicates(),
                                 on='clean_skill', how='left')

    # - Add to referential
    skills_referential = pd.concat([skills_referential, df_low_freq_typos])

    # Get non-typo skills
    df_low_freq_left = df_unmatch_other[df_unmatch_other.clean_found_skill == '']

    del df_tmp_typos, df_low_freq_typos, existing_ref_list, existing_ref_df

    # -------------------------------------------- #
    # --- Unify different skills due to spaces --- #

    existing_ref_df = skills_referential[skills_referential.flag >= 1]
    existing_ref_df = existing_ref_df[existing_ref_df.clean_skill != '']

    list_same_enrich = nf.is_one_space_away(list(set(existing_ref_df.enrich_skill)))
    list_same_clean = nf.is_one_space_away(list(set(existing_ref_df.clean_skill)))

    # Correction of spaces in referential
    skills_referential['clean_skill'].replace(dict(list_same_clean), inplace=True)
    skills_referential['enrich_skill'].replace(dict(list_same_enrich), inplace=True)

    # Let it go
    del list_same_enrich, list_same_clean, existing_ref_df

    # ---------------------------------------------------- #
    # --- Find acronyms in skills, update referentials --- #

    # - Prepare context and acronyms
    # Get referential usable values
    skills_referential_s = skills_referential.groupby(['raw_skill', 'clean_skill', 'enrich_skill']).agg(
        {'isinitref': 'max', 'flag': 'max'}).reset_index()
    skills_referential_s = skills_referential_s[skills_referential_s.flag >= 1]

    # Work with a subset of df_network
    df_skills_s = df_network[(df_network['sourceable_node_type'] == "User") &
                             (df_network['targetable_node_type'] == "Skill")]
    df_skills_s = df_skills_s[['sourceable_node_id', 'skill_name']]

    # Map referential onto df_skills_s et get enriched skills (same function for apply process only)
    df_skills_s = nf.apply_referential(df_skill=df_skills_s, col_skill='skill_name',
                                       df_referential=skills_referential_s, col_ref_from='raw_skill',
                                       col_ref_to='clean_skill')

    # Extract clean_skills with only 2 or 3 characters (potential acronyms)
    df_ref_potential_acron = skills_referential[(skills_referential.clean_skill.str.len() <= 3) &
                                                (skills_referential.clean_skill.str.len() >= 2) &
                                                (skills_referential.flag >= 1)]

    # List of skills which are not potential acronyms. x == x remove potential NaN values
    list_skills = list(set([x for x in list(skills_referential_s.clean_skill) if x == x
                            if x not in list(df_ref_potential_acron.clean_skill)]))

    potential_meanings = [(x, nf.list_matching_acron_texts(x, list_skills))
                          for x in list(set(list(df_ref_potential_acron.clean_skill)))]

    potential_meanings = [x for x in potential_meanings if len(x[1]) > 0]

    # - Quick reshaping to match 1st version shape (to prevent re-writing context functions)
    df_skills_ls = df_skills_s.groupby('sourceable_node_id')['skill_name'].apply(list).reset_index()

    # - Identify best meanings
    meanings_for_acronyms = []
    for acronym, meanings in potential_meanings:
        # Contexts of potential meanings
        contexts_meanings = nf.average_context(meanings, df_skills_ls, 'skill_name')

        # Contexts of the acronym
        context_acronym = nf.average_context([acronym], df_skills_ls, 'skill_name')

        if len(context_acronym) > 0:
            context_acronym = context_acronym[0][1]

        if len(contexts_meanings) > 0 and len(context_acronym) > 0:
            # Get most likely meaning of the acronym
            meanings_for_acronyms.append(nf.most_likely_meaning(acronym, context_acronym, contexts_meanings))

    # Filter on probability (20% for tri-gram acronyms, 30% for bi-gram acronmys, arbitrary), store in a dataframe
    meanings_for_acronyms_ls = [x for x in meanings_for_acronyms
                                if (len(x[0]) == 3 and x[2] > 0.2) or (len(x[0]) == 2 and x[2] > 0.3)]
    meanings_for_acronyms_keep_df = pd.DataFrame(meanings_for_acronyms_ls, columns=['acronym', 'meaning', 'proba'])

    # - Add columns
    # Set to non-declared acronyms
    if len(meanings_for_acronyms_keep_df) > 0:
        meanings_for_acronyms_keep_df['declared'] = 0
        # Get and set counts of these acronyms
        df_flags = df_skills_s[df_skills_s.skill_name.isin(list(meanings_for_acronyms_keep_df.acronym))]
        df_flags = df_flags.groupby('skill_name').agg({"sourceable_node_id": 'count'}).reset_index()
        d_flag = pd.Series(df_flags.sourceable_node_id.values, index=df_flags.skill_name).to_dict()

        meanings_for_acronyms_keep_df['flag'] = meanings_for_acronyms_keep_df['acronym'].apply(
            lambda x: d_flag.get(x, 0))

        # Add to acronym referential
        acronyms_referential = pd.concat([acronyms_referential, meanings_for_acronyms_keep_df])

    # - Replace acronym in referential
    # Reshape acronyms for replacement
    acronyms_referential = acronyms_referential.groupby(['acronym', 'meaning']).max().reset_index()  # WTF ?!
    acronyms_referential = acronyms_referential.sort_values(by=['acronym', 'proba'], ascending=[True, False])

    acronyms_referential_to_apply = acronyms_referential.drop_duplicates(keep='first', subset=['acronym'])

    acronyms_to_replace = list(zip(acronyms_referential_to_apply.acronym, acronyms_referential_to_apply.meaning))

    # Replace clean skill & enrich_skill

    skills_referential['clean_skill'].replace(dict(acronyms_to_replace), inplace=True)

    # Not optimal if the acronym is not in english..
    skills_referential['enrich_skill'].replace(dict(acronyms_to_replace), inplace=True)

    del acronyms_referential_to_apply, acronyms_to_replace, meanings_for_acronyms_keep_df, meanings_for_acronyms

    try:
        del potential_meanings, acronym, meanings, contexts_meanings, context_acronym, meanings_for_acronyms_ls
    except:
        pass

    # --------------------------------------- #
    # --- Standardized referential skills --- #

    skills_referential['stem_skill'] = skills_referential['enrich_skill'].apply(lambda x: nf.stemming_nltk(x))

    # ------------------------------ #
    # --- Search for stem skills --- #

    # Get quite sure skill (multiples occurrences and presence in LinkedIn referential)
    skills_referential_stem_ls = list(
        set(skills_referential[(skills_referential.flag >= 3) | (skills_referential.isinitref == 1)].stem_skill))

    # Get word with same stemmed form
    list_stem_match = [x for x in list(df_low_freq_left.clean_skill)
                       if nf.stemming_nltk(x) in skills_referential_stem_ls and len(x) > 5]

    # Create file to add
    df_jogl_stem = df_jogl_skills[df_jogl_skills.clean_skill.isin(list_stem_match)]
    df_jogl_stem["stem_skill"] = df_jogl_stem['clean_skill'].apply(lambda x: nf.stemming_nltk(x))

    # - Prepare a correction for clean & enrich skills based on stem
    df_referential_stem_match = skills_referential[skills_referential.stem_skill.isin(df_jogl_stem["stem_skill"])]

    # Get columns of interest
    df_referential_stem_match = df_referential_stem_match[["clean_skill", "enrich_skill", "flag", "stem_skill"]]
    df_referential_stem_match = df_referential_stem_match.groupby(
        ["clean_skill", "enrich_skill", "stem_skill"]).sum().reset_index()

    df_referential_stem_match = df_referential_stem_match.sort_values(by=['stem_skill', 'flag'],
                                                                      ascending=[True, False])

    df_referential_stem_match = df_referential_stem_match.drop_duplicates(keep='first', subset=['stem_skill'])

    # Merge back both dataframes
    df_jogl_stem = pd.merge(df_jogl_stem[['raw_skill', 'flag', 'stem_skill']],
                            df_referential_stem_match[['clean_skill', 'enrich_skill', 'stem_skill']],
                            on='stem_skill', how='left')

    print(skills_referential[skills_referential.raw_skill == "GCP"])
    skills_referential = pd.concat([skills_referential, df_jogl_stem])

    # ------------------------------------ #
    # ---  Final clean of referential  --- #

    #try:
    #    skills_referential['lg_skill'] = skills_referential['lg_skill'].fillna('unk')
    #except KeyError:
    #    print("no lg_skill")
    #    pass

    skills_referential['isinitref'] = skills_referential['isinitref'].fillna(0)

    skills_referential = skills_referential.dropna()

    skills_referential = skills_referential.groupby(['raw_skill', 'stem_skill', 'clean_skill', 'enrich_skill']).agg(
        {'isinitref': 'max', 'flag': 'max'}).reset_index()

    # ------------------------------------ #
    # ---  Final clean of referential  --- #

    store.skills_referential = skills_referential
    store.acronyms_referential = acronyms_referential

    store.save_skills_referential()
    store.save_acronyms_referential()


def wrap_applying_referential(store):
    """

    :param store:
    :return:
    """

    # --------------------------------- #
    # --- Get referential  & inputs --- #

    if len(store.skills_referential) > 0:
        skills_referential = store.skills_referential
    else:
        skills_referential = store.load_skills_referential()

    if len(store.skills) > 0:
        df_skills = store.skills
    else:
        df_skills = store.load_skills()

    if len(store.network) > 0:
        df_network = store.network
    else:
        df_network = store.load_network()

        # ---------------------------------- #
    # --- Create new index of Skills --- #

    df_skills['skill_name'] = np.where(df_skills.skill_name.isna() == True,
                                       'noskillpresent', df_skills.skill_name)

    # - Preprocess skills

    # Split multiple skills based on separators and reshape dataframe (1 skill per row)
    df_skills['extend_skill_name'] = df_skills['skill_name'].apply(lambda x: nf.split_enumeration(x))
    df_skills = df_skills.explode('extend_skill_name',
                                  ignore_index=True)  # see https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.explode.html

    # Split share subpart or combination of skills and reshape dataframe (1 skill per row).
    # E.g "bash/shell script" -> "bash script", "shell script" or "ci/cd" -> "ci", "cd"
    df_skills['extend_skill_name'] = df_skills['extend_skill_name'].apply(lambda x: nf.recreate_full_subskill(x))
    df_skills = df_skills.explode('extend_skill_name', ignore_index=True)

    # - Apply Referential of skills
    df_skills = df_skills.merge(skills_referential[['raw_skill', 'stem_skill']],
                                left_on='extend_skill_name', right_on='raw_skill', how='left')

    # - For unmatch skills (low frequence, new skill, etc.) apply basic cleaning. Since its uncommon skill, cleaning doesnt really matter

    # Soft cleaning of JOGL skills
    df_skills['stem_skill'] = df_skills.apply(lambda x: x['stem_skill'] if pd.isnull(x['stem_skill']) == False
    else nf.rem_trim_lower_str(x['extend_skill_name'], REPLACE_LS), axis=1)

    # - Compute unique index for new skills

    # Prepare df
    df_skills = df_skills[['id', 'stem_skill']]
    df_skills = df_skills.drop_duplicates(keep='first')  # just in case
    df_skills.columns = ['old_id', 'stem_skill']

    # Get unique values of stem skills and assign an unique number
    df_ids = df_skills[['stem_skill']].drop_duplicates(keep='first').reset_index()
    df_ids['id'] = df_ids.index

    d_ids = pd.Series(df_ids.id.values, index=df_ids.stem_skill).to_dict()

    # Apply new index
    df_skills['id'] = df_skills['stem_skill'].apply(lambda x: d_ids.get(x, -1))

    # -------------------------------- #
    # --- Apply new ids on Network --- #

    # - Preprocess ids old/new. stem_skills are not needed here
    df_ids = df_skills[['old_id', 'id']].groupby('old_id')['id'].apply(list).reset_index()
    d_ids = pd.Series(df_ids.id.values, index=df_ids.old_id).to_dict()

    # Get specific rows of the network data
    df_network_clean = df_network.copy()
    df_network_clean = df_network_clean[(df_network_clean.targetable_node_type == "Skill") &
                                        (df_network_clean.relation_type == "has_skill")]

    df_network_clean['targetable_node_id'] = df_network_clean['targetable_node_id'].apply(lambda x: d_ids.get(x, [-1]))
    df_network_clean = df_network_clean.explode('targetable_node_id', ignore_index=True)
    df_network_clean['targetable_node_type'] = 'CleanSkill'
    df_network_clean['relation_type'] = 'has_clean_skill'

    # -------------------------------- #
    # --- Output format for Skills --- #

    df_skills_clean = df_skills[['id', 'stem_skill']]
    df_skills_clean = df_skills_clean.drop_duplicates(keep='first')
    df_skills_clean.columns = ['id', 'skill_name']

    # ----------------------------- #
    # ---  Save result in Store --- #

    store.skills = df_skills_clean
    store.save_skills()

    store.network = df_network_clean
    store.save_network
