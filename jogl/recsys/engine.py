import click
import hinpy
import numpy as np
import sys

from ttictoc import Timer
from datetime import datetime

from .config import Config

class JoglRecommender:
    def __init__(self, source_store, target_store, config):
        '''
        Intializing recomender class
        '''
        if not source_store:
            raise Exception(
                "Error: You need to give the recommender engine a datastore.")
        if not config:
            raise Exception(
                "Error: You need to give the recommender engine a config.")
        self.source_store = source_store
        self.target_store = target_store
        self.config = config

        self.setup()

    def setup(self):
        '''
        Create the HIN object.
        '''
        data = self.source_store.load_data()

        self.hin = hinpy.classes.HIN(
            table=data, inverse_relations=True, verbose=False)

    def make_recos(self, source, target, top_k, meta_paths, weights, base_relation, new_relation, seen_relation):
        '''
        Make a set of recommendations for a given set of meta-paths.
        '''
        # We define a dictionary with the parameters
        params = {
            'method': 'CB',  # <- is the meta-path-based RS
            # <- number of items to propose in recommendation for each entity (typically users)
            'topK_predictions': top_k,
            # <- a relation that stores things users has seen, or chosen, so as to not recommend again
            'seen_relation': seen_relation,
            # This is the semantic part: list of meta-paths (list of link groups) to include
            'paths': meta_paths,
            'paths_weights': weights
        }

        # Computing recommendations is done by creating a new relation that
        # each recommendation as an edge (connecting a User with a Community)
        self.hin.CreateLinkGroupFromRS(
            # this is the base relation from which to compute recos using meta-paths
            relation_name=base_relation,
            new_relation_name=new_relation,  # the name of the new relation containing recos
            parameters=params)

        # retrieve a table with the recommendations
        recos = self.hin.table[self.hin.table.relation ==
                               new_relation].copy(deep=True)

        # add scores
        mixed_scores = np.sum([self.hin.proportional_abundances(
            path=mp) * w for mp, w in zip(meta_paths, weights)])
        source_positions = self.hin.GetObjectGroupPositionDic(source)
        target_positions = self.hin.GetObjectGroupPositionDic(target)
        scores = recos.apply(
            lambda x: mixed_scores[source_positions[x.start_object], target_positions[x.end_object]], axis=1)
        recos['value'] = scores
        recos['timestamp'] = datetime.now()

        return recos

    def make_all_recos(self):
        '''
        Make all recommendations specified in the configuration and add results to the store.
        '''
        for (name, reco) in self.config.get_recos():
            click.echo(
                'Will make top {} {} recommendations.'.format(reco['top_k'], name))

            with Timer(verbose=False) as T:
                recos = self.make_recos(
                    reco['source'],
                    reco['target'],
                    reco['top_k'],
                    [mp['path'] for mp in reco['meta_paths']],
                    [mp['weight'] for mp in reco['meta_paths']],
                    base_relation=reco['base_relation'],
                    new_relation=reco['new_relation'],
                    seen_relation=reco['seen_relation']
                )

            click.echo('{} recommendations in {:.1f}s ({:.1f} recommendations/s)'.format(
                len(recos), T.elapsed, len(recos) / T.elapsed))

            self.target_store.add_results(recos)
