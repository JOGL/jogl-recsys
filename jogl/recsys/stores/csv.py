import pandas as pd

from jogl.recsys.stores import to_hinpy, to_jogl, Store


class CsvFile(Store):
    def __init__(self, csv_file):
        super().__init__()
        self.csv_file = csv_file

    def load_data(self):
        # Read recsys_data table into a DataFrame
        df = pd.read_csv(self.csv_file)

        # Rename DataFrame columns to what is expected by hinpy
        df.rename(to_hinpy, axis=1, inplace=True)

        self.data = df
        return df

    def save_results(self):
        if len(self.results) > 0:
            # rename columns
            df = self.results.rename(to_jogl, axis=1)
            df['created_at'] = df['updated_at']

            # save results to result_path
            df.to_csv(self.csv_file, index=False)
