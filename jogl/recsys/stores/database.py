import pandas as pd
import os

from sqlalchemy import create_engine

from jogl.recsys.stores import to_hinpy, to_jogl, Store
from jogl.nlp.stores import DatabaseStore
from jogl.nlp import engine


class JoglDatabase(Store):
    def __init__(self, network_id=None):
        '''
        Initialize the JoglDatabase instance.  The DATABASE_URL environment
        variable must be defined.
        '''
        super().__init__()
        self.engine = create_engine(os.environ['DATABASE_URL'])
        self.network_id = network_id

    def preprocess_relations(self, row):
        type_node1 = row["start_group"]
        type_node2 = row["end_group"]
        relation = row["relation"]
        relation = "{}_{}_{}".format(type_node1, relation, type_node2)
        return relation

    def load_data(self):
        '''
        Build a pandas DataFrame from the recsys_data table which has the following
        structure:

        id type: bigint
        sourceable_node_type type: string
        sourceable_node_id type: string
        targetable_node_type type: string
        targetable_node_id type: string
        relation_type type: string
        value type: float

        We use a mapping to rename columns to the names expected by HINPy.
        '''
        with self.engine.connect() as connection:
            # Read recsys_data table into a DataFrame
            df = pd.read_sql_table('recsys_data', connection)

            # Rename DataFrame columns to what is expected by hinpy
            df.rename(to_hinpy, axis=1, inplace=True)
            df['relation'] = df.apply(self.preprocess_relations, axis=1)
            del df['created_at']
            del df['id']
            nlp_store = DatabaseStore()
            NLPEngine = engine.NLPEngine(nlp_store)
            NLPEngine.fit()
            NLPEngine.predict()
            nlp_store.network.rename(to_hinpy, axis=1, inplace=True)
            nlp_store.network['relation'] = nlp_store.network.apply(self.preprocess_relations, axis=1)
            del(nlp_store.network['created_at'])
            del(nlp_store.network['id'])
            df = df.append(nlp_store.network)
            self.data = df
            return df

    def save_results(self):
        '''
        Saves the dataframes passed as arguments to the recsys_results table.
        Previous data in the recsys_results table is removed. DataFrame's will
        have their columns renamed to what is expected by the recsys_results table.
        '''

        # TODO in future times
        # This method is a bit bruteforce as it destroys all records and overrides them with new ones
        # Moreover it has a lag between the delete from recsys_results and the to_sql
        # This will result in a database downtime period of increasing lenght as the number of objects to write increases
        # Better SQL wizardry will be needed at some point.
        ########################################################
        if self.network_id is None:
            raise Exception(
                'Saving database store results requires specifying a network_id')

        if len(self.results) > 0:
            df = self.results.rename(to_jogl, axis=1)
            df['created_at'] = df['updated_at']
            if self.network_id is not None:
                df['network_id'] = self.network_id
            with self.engine.connect() as connection:
                with connection.begin():
                    # Remove any existing results for this network_id
                    connection.execute(
                        f"delete from recsys_results where network_id = '{self.network_id}'")

                    # Write results to db
                    df.to_sql('recsys_results', connection,
                              index=False, if_exists='append')
