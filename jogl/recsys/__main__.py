import click
import sys

from jogl.recsys import RecommenderEngine
from jogl.recsys.config import Config
from jogl.recsys.stores import CSVStore, NetworkApiStore, DatabaseStore
from ttictoc import Timer

# TODO: figure out how to add help="Defines the path to the CSV file to be used as a datasource."
# to the csv_file so that it appears in the help menu !


@click.command(context_settings=dict(ignore_unknown_options=False))
@click.option('-c', '--config', type=click.Path(exists=True), required=True)
@click.option('-s', '--source', type=click.Choice(['api', 'csv', 'database']), default='api', help='Sets the type of source to get data from. Default: api')
@click.option('-t', '--target', type=click.Choice(['csv', 'database']), default='database', help='Sets the type of target to put data to. Default: database')
@click.option('-i', '--input', type=click.Path(exists=True), required=False, help='Input CSV file if the `--source csv` flag is present')
@click.option('-o', '--output', type=click.Path(exists=False), required=False, help='Output CSV file if the `--target csv` flag is present')
def main(source, target, config, input, output):
    '''
    JOGL Recommender System

    Learn more at https://jogl.io
    '''

    click.echo('JOGL Recommender System - Learn more at https://jogl.io')

    with Timer(verbose=False) as T:
        # This is the store selection switch
        # allowing for multiple type of data to be used
        # This will make it easy for data science folks to test
        # and implement new path locally.
        if source == 'api':
            click.echo('Will use the JOGL Network Api as data source.')
            source_store = NetworkApiStore()
        elif source == 'csv':
            if not input:
                raise Exception('Source CSV file is missing!')
            click.echo(
                f'Will use the {input} csv file as data source.')
            source_store = CSVStore(input)
        elif source == 'database':
            click.echo('Will use the JOGL Database as data source.')
            source_store = DatabaseStore()
        else:
            click.echo('Unknown data source: {}'.format(source))
            sys.exit(2)

        if target == 'csv':
            if not output:
                raise Exception('Target CSV file is missing!')
            click.echo(
                f'Will use the {output} csv file as data target.')
            target_store = CSVStore(output)
        elif target == 'database':
            click.echo('Will use the JOGL Database as data target.')
            target_store = DatabaseStore(
                source_store.get_network_id() if source == 'api' else None)
        else:
            click.echo('Unknown data target: {}'.format(target))
            sys.exit(2)

        click.echo('Using config file {}'.format(config))
        config = Config(config)

        engine = RecommenderEngine(source_store, target_store, config)
        engine.make_all_recos()

        target_store.save_results()

    click.echo('Total elapsed time: {:.1f}s'.format(T.elapsed))


if __name__ == '__main__':
    # pylint: disable=no-value-for-parameter
    main()
