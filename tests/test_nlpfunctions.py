import numpy as np
import fasttext
import jogl.nlp.nlp_functions as nf

model = fasttext.load_model('jogl/nlp/models/lid.176.ftz')
dict_lg_models = nf.load_mar_models()

# --- Focus tests --- #
def test_language_detect_ft():
    assert nf.language_detect_ft("bonjour le monde", threshold=0.5) == 'fr'
    assert nf.language_detect_ft("engineering", threshold=0.5) == 'en'
    assert nf.language_detect_ft("lg lhg", threshold=0.5) == 'unk'


def test_language_transl_hf():
    assert nf.language_transl_hf("chef de projet", "fr", dict_lan_model=dict_lg_models,
                                 check_presence=None) == "project manager"
    assert nf.language_transl_hf("chef de projet", "fr", dict_lan_model=dict_lg_models,
                                 check_presence=['project manager']) == "project manager"
    assert nf.language_transl_hf("chef de projet", "fr", dict_lan_model=dict_lg_models,
                                 check_presence=['any other skills']) == "chef de projet"


def test_stemming_nltk__any_stem():
    assert nf.stemming_nltk("cleaning") == 'clean'
#tester l'erreur d'api ?


# --- Basic tests --- #
def test_rem_trim_lower_str__lower_and_trim():
    assert nf.rem_trim_lower_str("Hello ", other_replace_ls=None) == "hello"


def test_rem_parentheses__parentheses():
    assert nf.rem_parentheses("does it remove (this) ") == "does it remove"


def test_apply_function_to_list():

    def square(x):
        return x * x

    assert nf.apply_function_to_list([2, 3, 5], square) == [4, 9, 25]


def test_split_enumeration__pipes():
    assert nf.split_enumeration("skill1 / skill2 / skill5 / skill6") == ['skill1', 'skill2', 'skill5', 'skill6']
    assert nf.split_enumeration("skill1 - skill2 - skill5 - skill6") == ['skill1', 'skill2', 'skill5', 'skill6']
    assert nf.split_enumeration("skill1, skill2,skill5, skill6") == ['skill1', 'skill2', 'skill5', 'skill6']


def test_split_enumeration__all_cases():
    assert nf.recreate_full_subskill("ui/ux") == ['ui', 'ux']
    assert nf.recreate_full_subskill("ui/ux design") == ['ui design', 'ux design']
    assert nf.recreate_full_subskill("designer ui/ux") == ['designer ui', 'designer ux']
    assert nf.recreate_full_subskill("machine learning/deep learning") == ['machine learning', 'deep learning']


def test_flatten():
    flattened = nf.flatten([[1, 2], 3, [4], []])
    assert [x for x in flattened] == [1, 2, 3, 4]


def test_check_alpha_character():
    assert nf.check_alpha_character("alpha") is True
    assert nf.check_alpha_character(0) is False
    assert nf.check_alpha_character('') is False
    assert nf.check_alpha_character(np.nan) is False


def test_is_acronym_specified():
    assert nf.is_acronym_specified("gcp", "google cloud plat") is True
    assert nf.is_acronym_specified("gcp", "beginner") is False


def test_match_upper_number__number_uppercasecount():
    assert nf.match_upper_number("8", "unk") == 'en'
    assert nf.match_upper_number("MultipleUpper", "unk") == 'en'
    assert nf.match_upper_number("CAP", "unk") == 'en'


def test_get_typos():
    assert nf.get_typos("typi", ["typo", "levenshtein", "distance"]) == ''
    assert nf.get_typos("levenstein", ["typo", "levenshtein", "distance"]) == 'levenshtein'


def test_just_a_space__return_match_only():
    assert nf.just_a_space("withoutanyspace", "without any space") == ("withoutanyspace", "without any space")
    assert nf.just_a_space("without any space", "withoutanyspace") == ("withoutanyspace", "without any space")
    assert nf.just_a_space("without any spacess", "withoutanyspace") == ''


def test_is_one_space_away():
    assert set(nf.is_one_space_away(['my test', 'mytest', 'check'])) == {('check', 'check'), ('my test', 'my test'),
                                                                         ('mytest', 'mytest'), ('mytest', 'my test')}
#
