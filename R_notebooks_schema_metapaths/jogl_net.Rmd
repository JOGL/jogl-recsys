---
title: "JOGL Networks"
output: html_notebook
---

Trying to identify metapaths from the JOGL network and apply some relevant metrics!


0. Load the JOGL network data as .graphml (exported through the api). I have two versions locally - 26 and 27.

```{r}

library(igraph) #Library for graph/network modelling and algorithms
library(visNetwork) #Library to plot networks
library(plyr) #Library to process dataframes

g_27_w = read.graph("JOGL_network_27.graphml", format = "graphml")
g_26_w = read.graph("JOGL_network_26.graphml", format = "graphml")

recos = read.csv("com_recos.csv") #I am reading the output of the recommendation system here as a .csv file

#E(g_27_w)$relation = factor(E(g_27_w)$relation)

```

Custom Plot Function - from library visNetwork. Feel free to tweak the parameters for relevant visualizations

```{r}
visPlot = function(subgraph, communities = rep(1, length(V(subgraph))), nodesize = 0, edgewidth = 0, title="")
{
  nodes <- data.frame(id = V(subgraph)$name, group = communities)
  nodes$font.size<-20
  nodes$value = (nodesize)*3000 + 15
  edges <- data.frame(get.edgelist(subgraph))
  #edges$width = edgewidth
  colnames(edges)<-c("from","to")
  plt = visNetwork(nodes, edges, height = "600px", main = title)%>%
    visIgraphLayout(layout = "layout_nicely",randomSeed = 123) %>%
    visOptions(
               highlightNearest = TRUE) %>%
    visNodes(scaling = list(min = 5, max = 20)) %>%
    visEdges(arrows ="to") %>% visGroups(groupname = "2", color = list(background = "yellow",border = "orange")) %>%
    visInteraction(keyboard = T,
                   dragNodes = T, 
                   dragView = T, 
                   zoomView = T)
  return(plt)
}

```


Collapse Network - Looking at unique recoms (Schema)

```{r}
E(g_27_w)$weight = rep(1,length(E(g_27_w))) #Set weights of all edges as 1

g = contract.vertices(g_27_w, mapping = as.numeric(mapvalues(V(g_27_w)$type, from = unique(V(g_27_w)$type),to = 1:length(unique(V(g_27_w)$type)) )),vertex.attr.comb = list("ignore")) #Contracting vertices based on the "type" - project, post, need etc.

V(g)$name = unique(V(g_27_w)$type)

#Since there are multiple edges/relations between the same pair of nodes - I simplify them for now.

g_simp_or = simplify(g, remove.loops = F, remove.multiple = T) #Removing multiple (parallel) edges
g_simp_or = delete.vertices(g_simp_or, v = "") #Removing a stray vertex "" - do check if this is present in later network versions

#Split "User" node into two nodes to enable simple paths - with repetition (This is whe workaround for enabling user->user in metapaths, though repetition only twice)

edges = ends(g_simp_or, es = E(g_simp_or))
g_simp = add_vertices(g_simp_or, 1, attr = c(name = "User1"))

for (i in 1:nrow(edges))
{
  if (edges[i,1] == "User")
    g_simp = add_edges(g_simp, c("User1",edges[i,2]))
  
  if (edges[i,2] == "User")
    g_simp = add_edges(g_simp, c(edges[i,1], "User1"))
}

g_simp = simplify(g_simp, remove.loops = T) #Removing self loops

#visPlot(g_simp, nodesize = degree(g_simp)) #Plotting the Schema

visPlot(g_simp_or, nodesize = degree(g_simp_or)) #Plotting the Schema - this one has only one "User" vertex
```

As a reference - creating a dataframe for all relations between entities and their weights

```{r}

#Weight of each Relation

edges = ends(g_simp_or, es = E(g_simp_or))
weights = data.frame()

for (i in 1:nrow(edges))
{
  subg = induced_subgraph(g_27_w, v = V(g_27_w)[V(g_27_w)$type %in% c(edges[i,1],edges[i,2])])
  rel = unique(E(subg)$relation)
  
  for (j in rel)
  {
    weights = rbind(weights, data.frame(Relation = j, Start = edges[i,1], end = edges[i,2], weight = length(E(subg)[E(subg)$relation == j])))
  }
  
}

g_weights = graph_from_data_frame(weights[,c(2,3,1,4)], directed = T) #A network with the parallel edges for each relation

```


Recommending X->Y means there is definitely an actionable outcome by X onto Y [User to community means the user can do an action towards a community]. Thus all recommendations are what constitutes a directed edge in the simplified DAG. But not all entities are live. So recommendations are done to a user based on other entities. 

Alternatively, recommendations can be done to groups of special users (who lead an entity (project, posting needs)) to other users. 


List Paths in the Schema

Since the recommender engine has "inverse" relations - I compute simple paths including all edges (in/out) using an iGRAPH function and then shortlist only the paths of length 3/4.

```{r}

# A small custom recursive function to compute paths from start to end vertices within specified length "k". Do mind that I am using all directions of edges (since we have inverse relations in the HIN). Paths are printed in reverse order. 

k_simple_paths = function(graph, from, to, k)
{
  if (!is_igraph(graph)) 
		stop("Not a graph object")
  
  paths = c()
  
  for (i in neighbors(graph, from, mode = "all")$name)
  {
    
    if (i == to & k>0)
    {
      paths = c(paths, list(c(i,from)))
    }
      
    else if (!(i == to) & k>0 )
    {
      path = k_simple_paths(graph, i, to, k-1)
      
      for (j in path)
      {
        if (to %in% j)
        {
          temp = c(j,i,from)
          paths = c(paths,list(temp)) 
        }
      }
    }
    
    else
      return(NA)
  }
  
  return(paths[!duplicated(paths)])
  
}
```

Now - compute all paths from User (since we recommend to a user - modify if there are alternate recommendations)

```{r}

paths = list()
g_simp_or = simplify(g_simp_or, remove.loops = T, remove.multiple = F)

for (i in neighbors(g_simp_or, v = "User")$name)
{
  temp = k_simple_paths(g_simp_or, from = "User", to = i, k = 3) #Setting k as atmost 3 - I don't think longer recommendations are useful
  
  for (j in temp)
  {
    df = as.data.frame(matrix(j, ncol = 2, byrow = T))
    df = df[order(nrow(df):1),c(2,1)]
    
    rownames(df) = c(1:nrow(df))
    colnames(df) = c("From", "To")
   
    paths = append(paths, list(df))
     
  }
  
}

```

Now "paths" is a list of dataframes - one for each unique path starting from user. Since there are multiple edge relations - a simple merge with the "weights" dataframe can result in all possible paths individually.

Till above is a more optimal way to compute the metapaths. A lot of the reported metapaths have way less representation in the HIN in terms of edge weight. It would be better to eliminate them

**************************************************************************************************************************************
**************************************************************************************************************************************

Here - I am using an earlier old+slow approach. I compute for all pairs of start and end nodes - all possible simple paths between them. Finally I prune paths whose length is less than 3 and 4. Takes way longer to execute - but function is inbuilt. You could add another level of pruning by limiting the start nodes.

(Skip till the next double asterisk lines - but the code executes, if you are curious)

```{r}

p3 = data.frame() #For paths of length 3
p4 = data.frame() #For paths of length 4

entities = V(g_simp)$name

pairs = combn(entities, m = 2)
pairs = data.frame(from = pairs[1,], to = pairs[2,])


for (i in 1:nrow(pairs))
{
  temp = all_simple_paths(g_simp, from = pairs$from[i], to = pairs$to[i], mode = "all") 
  #print(temp)
  
  for (j in temp)
  {
    if (length(j) == 4)
    {
      #print(j)
      p4 = rbind(p4, data.frame(start = j$name[1], med1 = j$name[2], med2 = j$name[3], end = j$name[4]))
    }
    
    else if (length(j) == 3)
    {
      p3 = rbind(p3, data.frame(start = j$name[1], med = j$name[2], end = j$name[3]))
    }
  }
  
}

#p3[p3 == "User1"] = "User"
#p4[p4 == "User1"] = "User"


```


Pruning Uninteresting Metapaths i.e - Only metapaths starting from a user [Play around to experiment with other metapaths]

```{r}

p3 = p3[p3$start == "User",]
p4 = p4[p4$start == "User",]

```

**************************************************************************************************
**************************************************************************************************

Set up Metrics *Literature To Evaluate the paths - check https://arxiv.org/pdf/2001.01296.pdf (I have been reading this - but never got around to implementing the metrics)

I see that you would be better off using the "HINpy" python library (associated with the paper for evaluating metapaths). It has loads of metrics presented in the paper. I have not tried using that in python.

Trivial Metrics:

1. Flow through a metapath - for each entities - I select the ones with maximum edgeweights

```{r, message=F, warning=F}

dummy_flow = function(weights, start, end)
{
  a = max(weights$weight[weights$Start == start & weights$end == end])
  b = max(weights$weight[weights$Start == end & weights$end == start])
  
  if (a>b)
    return(c(a, as.character(weights$Relation[weights$weight == a][[1]]), F))
  else
    return(c(b, as.character(weights$Relation[weights$weight == b][[1]]), T))
}

max_flow = list()

for (i in paths)
{
  i$flow = 0
  i$relation = NA
  i$inverse = NA

  for (j in 1:nrow(i))
  {
    temp = dummy_flow(weights, start = as.character(i$From[j]), end = as.character(i$To[j]))
    i$flow[j] = temp[[1]]
    i$relation[j] = temp[[2]]
    i$inverse[j] = temp[[3]]
  }
  
  max_flow = append(max_flow, list(i))
}


```

The final dataframe "max_flow" has for all metapaths - the specific edge attribute that occurs with the most frequency. This is by no means the "best" metapath - that would be a combination of several metapaths with weights. I recommend moving the analysis to python (https://github.com/pedroramaciotti/HINPy) since the HINpy library offers more flexibility, existing metrics to compute metapaths and metrics. 

I wrote this code as a simple guide to visualize the schema and play around with things a bit


*************************************************************************************************************

Some Example Network Plots

```{r}
g_27 = induced.subgraph(g_27_w, vids = V(g_27_w)[V(g_27_w)$type %in% c("User","Community")])
```


Relation - Has Followed

```{r}
g_hf = subgraph.edges(g_27, eids = E(g_27)[E(g_27)$relation == "has_followed"] )
V(g_hf)$name = V(g_hf)$id
visPlot(g_hf, communities = V(g_hf)$type, nodesize = betweenness(g_hf))
```

```{r}
g_hc = subgraph.edges(g_27, eids = E(g_27)[E(g_27)$relation == "has_clapped"] )
V(g_hc)$name = V(g_hc)$id
visPlot(g_hc, communities = V(g_hc)$type, nodesize = degree(g_hc))
```


```{r}
g_hv = subgraph.edges(g_27, eids = E(g_27)[E(g_27)$relation == "has_visited"] )
V(g_hv)$name = V(g_hv)$id
visPlot(g_hv, communities = V(g_hv)$type,nodesize = betweenness(g_hv))
```

```{r}
g_27 = induced.subgraph(g_27_w, vids = V(g_27_w)[V(g_27_w)$type %in% c("User", "Skill", "Community")])
```


```{r}
g_hs = subgraph.edges(g_27, eids = E(g_27)[E(g_27)$relation == "has_skill"] )
V(g_hs)$name = V(g_hs)$id
visPlot(g_hs, communities = V(g_hs)$type)
```


Plot Degree Distribution

```{r}

library(plotly)

dd_hf = degree.distribution(g_hf, cumulative = T, mode = "in")
dd_hc = degree.distribution(g_hc, cumulative = T, mode = "in")
#dd_hs = degree.distribution(g_hs, cumulative = T, mode = "in")
dd_hv = degree.distribution(g_hv, cumulative = T, mode = "in")

df= data.frame()

df = rbind(df, data.frame(x = 1:length(dd_hf), dist = dd_hf, flag = "has_followed", alpha = fit_power_law(degree(g_hf, mode = "in"))$alpha[[1]]))
df = rbind(df, data.frame(x = 1:length(dd_hc), dist = dd_hc, flag = "has_clapped", alpha = fit_power_law(degree(g_hc, mode = "in"))$alpha[[1]]))
#df = rbind(df, data.frame(x = 1:length(dd_hs), dist = dd_hs, flag = "has_skill", alpha = fit_power_law(degree(g_hs, mode = "in"))$alpha[[1]]))
df = rbind(df, data.frame(x = 1:length(dd_hv), dist = dd_hv, flag = "has_visited", alpha = fit_power_law(degree(g_hv, mode = "in"))$alpha[[1]]))

p <- plot_ly()%>%
  layout(title = "Log-Log of Degree Distribution",
         xaxis = list(title = "In - Degree"),
         yaxis = list (title = "") )

for (i in unique(df$flag))
{
  p <- p %>% add_trace(x = df$x[df$flag == i], y = df$dist[df$flag == i], name = i,type = "scatter", mode = 'lines+markers')
}

 p = layout(p, xaxis = list(type = "log"), yaxis = list(type = "log"))
 show(p)

plt = ggplot(df, aes(x = x, y = dist)) + geom_line(aes(color = flag)) + theme_bw(base_size = 15) + ylab("Cumulative Degree Distribution")
ggplotly(plt)
```





