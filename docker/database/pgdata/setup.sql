CREATE TABLE recsys_data (
  id INT NOT NULL,
  sourceable_node_type VARCHAR(1000),
  sourceable_node_id INT,
  targetable_node_type VARCHAR(1000),
  targetable_node_id INT,
  relation_type VARCHAR(1000),
  value FLOAT DEFAULT 0.0,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);

CREATE TABLE recsys_results (
  id INT NOT NULL,
  sourceable_node_type VARCHAR(1000),
  sourceable_node_id INT,
  targetable_node_type VARCHAR(1000),
  targetable_node_id INT,
  relation_type VARCHAR(1000),
  value FLOAT,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);

CREATE TABLE skills (
  id INT NOT NULL,
  skill_name VARCHAR(1000),
  clean_skill_name VARCHAR(1000),
  enriched_skill_name VARCHAR(1000),
  stemmed_skill_name VARCHAR(1000),
  is_linkedin_skill BOOLEAN,
  count INT,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);

CREATE TABLE acronyms (
  id INT NOT NULL,
  acronym_name VARCHAR(1000),
  probable_skill_name VARCHAR(1000),
  probability FLOAT,
  is_in_parenthesis BOOLEAN,
  count INT,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);

CREATE TABLE recsys_skills_referential (
  id INT NOT NULL,
  skill_name VARCHAR(1000),
  clean_skill_name VARCHAR(1000),
  enriched_skill_name VARCHAR(1000),
  stemmed_skill_name VARCHAR(1000),
  lg_skill VARCHAR(100),
  is_linkedin_skill BOOLEAN,
  count INT,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
)

\copy recsys_data FROM '/data/recsys_datum_export.csv' DELIMITER ',' CSV HEADER;
\copy skills FROM '/data/skills_datum_export.csv' DELIMITER ',' CSV HEADER;
\copy acronyms FROM '/data/acronyms_datum_export.csv' DELIMITER ',' CSV HEADER;
