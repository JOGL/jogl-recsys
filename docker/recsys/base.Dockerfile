# Base docker image
#FROM python:3.9
FROM registry.gitlab.com/jogl/jogl-recsys/base:latest

# Use app dir
WORKDIR /usr/src/app

# Install dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY ./jogl ./jogl

RUN python -c "import jogl.nlp.nlp_functions as nf; nf.load_mar_models()"
