#!/bin/sh

# Adapted from https://toedter.com/2018/06/02/heroku-docker-deployment-update/

docker pull $RECSYS_IMAGE
docker tag $RECSYS_IMAGE registry.heroku.com/$HEROKU_APP/recsys
echo $HEROKU_TOKEN | docker login -u _ --password-stdin registry.heroku.com
docker push registry.heroku.com/$HEROKU_APP/recsys
apk add --no-cache curl

imageId=$(docker inspect registry.heroku.com/$HEROKU_APP/recsys --format={{.Id}})
payload='{"updates":[{"type":"recsys","docker_image":"'"$imageId"'"}]}'

curl -n -X PATCH https://api.heroku.com/apps/$HEROKU_APP/formation \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_TOKEN"
